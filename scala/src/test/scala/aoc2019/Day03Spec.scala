package aoc2019

import org.scalatest._

class Day03Spec extends FlatSpec with Matchers {
  "Day03 " should " properly parse the input " in {
    Day03.parse(List("R1,U2,D3,L4","U3,R2")) shouldEqual Seq(Seq("R1", "U2", "D3", "L4"), Seq("U3", "R2"))
  }
  "Day03 " should " properly compute part 1 " in {
    // val mem1 = Array(1,9,10,3,2,3,11,0,99,30,40,50)
    // VMState(0, mem1).run shouldEqual VMState(8, mem1.updated(3, 70).updated(0, 3500))
    // val mem2 = Array(1,0,0,0,99)
    // VMState(0, mem2).step shouldEqual VMState(4, mem2.updated(0, 2))
    // val mem3 = Array(2,3,0,3,99)
    // VMState(0, mem3).step shouldEqual VMState(4, mem3.updated(3, 6))
    // val mem4 = Array(2,4,4,5,99,0)
    // VMState(0, mem4).step shouldEqual VMState(4, mem4.updated(5, 9801))
    // val mem5 = Array(1,1,1,4,99,5,6,0,99)
    // VMState(0, mem5).run shouldEqual VMState(8, mem5.updated(0, 30).updated(4, 2))
  }
}
