package aoc2019

import org.scalatest._

class Day01Spec extends FlatSpec with Matchers {
  "Day01 " should " parse the input " in {
    Day01.parse(List("1", "2")) shouldEqual List(1, 2)
    Day01.parse(List("134", "23", "53")) should not be List(1, 2)
  }

  "Day01 " should " properly compute part 1 " in {
    Day01.eval1(12) shouldEqual 2
    Day01.eval1(14) shouldEqual 2
    Day01.eval1(1969) shouldEqual 654
    Day01.eval1(100756) shouldEqual 33583
    Day01.part1(List(12, 14, 1969, 100756)) shouldEqual (2+2+654+33583)
  }

  "Day01 " should " properly compute part 2 " in {
    Day01.eval2(12) shouldEqual 2
    Day01.eval2(14) shouldEqual 2
    Day01.eval2(1969) shouldEqual 966
    Day01.eval2(100756) shouldEqual 50346
    Day01.part2(List(12, 14, 1969, 100756)) shouldEqual (2+2+966+50346)
  }
}
