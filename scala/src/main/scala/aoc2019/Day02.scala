package aoc2019

import scala.io.Source

object Day02 extends App {
  case class VMState(val ip:Int, val memory:Seq[Int]) {
    def m(x:Int) = memory(x)
    def mm(x:Int) = memory(memory(x))
    def param(x:Int) = m(ip + x)
    def opcode() = m(ip)
    def done() = opcode() == 99
    def step() : VMState = opcode() match {
      case 1 => VMState(ip + 4, memory.updated(param(3), m(param(1)) + m(param(2))))
      case 2 => VMState(ip + 4, memory.updated(param(3), m(param(1)) * m(param(2))))
      case _ => this
    }
    def run() : VMState = {
      var vm = this
      while (!vm.done) vm = vm.step()
      vm
    }
    def w(idx:Int, x:Int) = VMState(ip, memory.updated(idx, x))
    def init(noun:Int, verb:Int) = w(1, noun).w(2, verb)
  }

  def load(path : String) : String = Source.fromFile(path).getLines().toList.mkString(",")
  def parse(line : String) : VMState = VMState(0, line.split(',').map(_.toInt))

  def part1(vm : VMState) = vm.init(12, 2).run().m(0)
  def part2(vm : VMState) : Int = {
    for (noun <- 0 to 99 ) {
      for (verb <- 0 to 99 ){
        if (vm.init(noun, verb).run().m(0) == 19690720) {
          return noun * 100 + verb
        }
      }
    }
    return -1
  }

  def execute() {
    val input = parse(load("../inputs/02.txt"))
    println(part1(input))
    println(part2(input))
  }

  execute
}