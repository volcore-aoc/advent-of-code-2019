package aoc2019

import scala.io.Source

object Day01 extends App {
  def load(path : String) : List[String] = Source.fromFile(path).getLines().toList
  def parse(lines : List[String]) : List[Int] = lines.map(_.toInt)

  def part1(lines : List[Int]) = lines.map(eval1).sum
  def part2(lines : List[Int]) = lines.map(eval2).sum

  def eval1(x : Int) : Int = x / 3 - 2
  def eval2(x : Int) : Int = eval1(x) match {
    case m if m <= 0 => 0
    case m => m + eval2(m)
  }

  def execute() {
    val input = parse(load("../inputs/01.txt"))
    println(part1(input))
    println(part2(input))
  }

  execute
}