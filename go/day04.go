package main

import (
	"fmt"
)

func checkNumber(number int) bool {
	hasAdjacent := false
	digit := number % 10
	remainder := number / 10
	for i := 0; i < 5; i++ {
		nextDigit := remainder % 10
		if digit == nextDigit {
			hasAdjacent = true
		} else if digit < nextDigit {
			return false
		}
		digit = nextDigit
		remainder = remainder / 10
	}
	if !hasAdjacent {
		return false
	}
	return true
}

func checkNumber2(number int) bool {
	adjacentCount := make([]int, 10)
	digit := number % 10
	remainder := number / 10
	for i := 0; i < 5; i++ {
		nextDigit := remainder % 10
		if digit == nextDigit {
			adjacentCount[digit]++
		} else if digit < nextDigit {
			return false
		}
		digit = nextDigit
		remainder = remainder / 10
	}
	hasAdjacent := false
	for i := 0; i < 10; i++ {
		hasAdjacent = hasAdjacent || (adjacentCount[i] == 1)
	}
	if hasAdjacent == false {
		return false
	}
	// Print the number
	fmt.Println(number)
	return true
}

func compute1(min int, max int) int {
	count := 0
	for i := min; i <= max; i++ {
		if checkNumber(i) {
			count++
		}
	}
	return count
}

func compute2(min int, max int) int {
	count := 0
	for i := min; i <= max; i++ {
		if checkNumber2(i) {
			count++
		}
	}
	return count
}

func main() {
	// checkNumber2(145889)
	fmt.Println(compute1(145852, 616942))
	fmt.Println(compute2(145852, 616942))
}
