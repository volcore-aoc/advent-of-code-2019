package main

import "fmt"

type intCodeVM struct {
	memMap        map[int64]int64
	ip            int64
	terminated    bool
	input         chan int64
	inputList     chan []int64
	output        chan int64
	done          chan bool
	relBaseOffset int64
	debug         bool
	inputBuffer   []int64
}

func createIntCodeVMBuffered(memory []int64, bufferSize int64) *intCodeVM {
	memMap := make(map[int64]int64)
	for idx, value := range memory {
		memMap[int64(idx)] = value
	}
	return &intCodeVM{memMap,
		0,
		false,
		make(chan int64, bufferSize),
		make(chan []int64, bufferSize),
		make(chan int64, bufferSize),
		make(chan bool),
		0,
		false,
		[]int64{},
	}
}

func createIntCodeVM(memory []int64) *intCodeVM {
	return createIntCodeVMBuffered(memory, 2)
}

func (vm *intCodeVM) mem(idx int64) int64 {
	return vm.memMap[idx]
}

func (vm *intCodeVM) setmem(idx int64, value int64) {
	vm.memMap[idx] = value
}

func (vm *intCodeVM) param(idx int64, mode int64) int64 {
	switch mode {
	case 0:
		{
			// reference
			return vm.mem(vm.mem(vm.ip + idx))
		}
	case 1:
		{
			// immediate
			return vm.mem(vm.ip + idx)
		}
	case 2:
		{
			// relative mode
			return vm.mem(vm.relBaseOffset + vm.mem(vm.ip+idx))
		}
	default:
		{
			fmt.Println("mode ", mode, " NYI")
			return 0
		}
	}
}

func (vm *intCodeVM) paramOut(idx int64, mode int64) int64 {
	switch mode {
	case 0:
		{
			// reference
			return vm.mem(vm.ip + idx)
		}
	case 1:
		{
			// immediate
			return vm.mem(vm.ip + idx)
		}
	case 2:
		{
			// relative mode
			return vm.relBaseOffset + vm.mem(vm.ip+idx)
		}
	default:
		{
			fmt.Println("mode ", mode, " NYI")
			return 0
		}
	}
}

func (vm *intCodeVM) step() {
	instruction := vm.mem(vm.ip)
	opCode := instruction % 100
	p1Mode := (instruction / 100) % 10
	p2Mode := (instruction / 1000) % 10
	p3Mode := (instruction / 10000) % 10
	if vm.debug {
		fmt.Println(instruction, opCode, p1Mode, p2Mode, p3Mode, vm.mem(vm.ip), vm.mem(vm.ip+1), vm.mem(vm.ip+2), vm.mem(vm.ip+3))
	}
	switch opCode {
	case 1:
		{
			vm.setmem(vm.paramOut(3, p3Mode), vm.param(1, p1Mode)+vm.param(2, p2Mode))
			vm.ip += 4
			break
		}
	case 2:
		{
			vm.setmem(vm.paramOut(3, p3Mode), vm.param(1, p1Mode)*vm.param(2, p2Mode))
			vm.ip += 4
			break
		}
	case 3:
		{
			if vm.debug {
				fmt.Print("Reading...")
			}
			value := int64(0)
			if len(vm.inputBuffer) > 0 {
				value, vm.inputBuffer = vm.inputBuffer[0], vm.inputBuffer[1:]
			} else {
				select {
				case value = <-vm.input:
					{
						break
					}
				case list := <-vm.inputList:
					{
						value = list[0]
						vm.inputBuffer = append(vm.inputBuffer, list[1:]...)
						break
					}
				}
			}
			vm.setmem(vm.paramOut(1, p1Mode), value)
			if vm.debug {
				fmt.Println(vm.mem(vm.paramOut(1, p1Mode)))
			}
			vm.ip += 2
			break
		}
	case 4:
		{
			if vm.debug {
				fmt.Println("Writing ", vm.param(1, p1Mode))
			}
			vm.output <- vm.param(1, p1Mode)
			vm.ip += 2
			break
		}
	case 5:
		{
			if vm.param(1, p1Mode) != 0 {
				vm.ip = vm.param(2, p2Mode)
			} else {
				vm.ip += 3
			}
			break
		}
	case 6:
		{
			if vm.param(1, p1Mode) == 0 {
				vm.ip = vm.param(2, p2Mode)
			} else {
				vm.ip += 3
			}
			break
		}
	case 7:
		{
			res := int64(0)
			if vm.param(1, p1Mode) < vm.param(2, p2Mode) {
				res = 1
			}
			vm.setmem(vm.paramOut(3, p3Mode), res)
			vm.ip += 4
			break
		}
	case 8:
		{
			res := int64(0)
			if vm.param(1, p1Mode) == vm.param(2, p2Mode) {
				res = 1
			}
			vm.setmem(vm.paramOut(3, p3Mode), res)
			vm.ip += 4
			break
		}
	case 9:
		{
			vm.relBaseOffset += vm.param(1, p1Mode)
			vm.ip += 2
			break
		}
	default:
		{
			fmt.Println("Unknown instruction ", instruction)
			vm.terminated = true
			close(vm.done)
			break
		}
	case 99:
		{
			vm.terminated = true
			close(vm.done)
			break
		}
	}
}

func (vm *intCodeVM) run() *intCodeVM {
	for {
		vm.step()
		if vm.terminated {
			return vm
		}
	}
}

func (vm *intCodeVM) readLine64() ([]int64, bool) {
	line := []int64{}
	for {
		select {
		case c := <-vm.output:
			{
				if c == 10 {
					return line, true
				}
				line = append(line, c)
				break
			}
		case <-vm.done:
			{
				return line, false
			}
		}
	}

}

func (vm *intCodeVM) readLine() (string, bool) {
	line := ""
	for {
		select {
		case c := <-vm.output:
			{
				if c == 10 {
					return line, true
				}
				line += string(rune(c))
				break
			}
		case <-vm.done:
			{
				return line, false
			}
		}
	}
}

func (vm *intCodeVM) writeLine(line string) {
	for _, r := range line {
		vm.input <- int64(r)
	}
	if line[len(line)-1] != 10 {
		vm.input <- int64(10)
	}
}
