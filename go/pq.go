package main

import (
	"container/heap"
	"fmt"
)

type pqNode struct {
	value interface{}
	score int
}

type prioQueue []*pqNode

func (pq prioQueue) Len() int {
	return len(pq)
}

func (pq prioQueue) Less(i, j int) bool {
	return pq[i].score < pq[j].score
}

func (pq prioQueue) Swap(i, j int) {
	pq[i], pq[j] = pq[j], pq[i]
}

func (pq *prioQueue) Push(x interface{}) {
	no := x.(*pqNode)
	*pq = append(*pq, no)
}

func (pq *prioQueue) Pop() interface{} {
	old := *pq
	n := len(old)
	no := old[n-1]
	*pq = old[0 : n-1]
	return no
}

func (pq *prioQueue) Next() *pqNode {
	return heap.Pop(pq).(*pqNode)
}

func (pq *prioQueue) Put(value interface{}, score int) {
	heap.Push(pq, &pqNode{
		value: value,
		score: score,
	})
}

func (pq pqNode) String() string {
	return fmt.Sprintf("%s @ %d", pq.value, pq.score)
}

func (pq prioQueue) String() string {
	s := ""
	for _, node := range pq {
		s += node.String() + ";"
	}
	return s
}
