package main

import (
	"fmt"
)

type vm struct {
	memory     []int
	ip         int
	terminated bool
	input      int
}

func createVM(memory []int, input int) *vm {
	return &vm{memory, 0, false, input}
}

func (vm *vm) param(idx int, mode int) int {
	if mode == 0 {
		return vm.memory[vm.memory[vm.ip+idx]]
	} else {
		return vm.memory[vm.ip+idx]
	}
}

func (vm *vm) step() {
	instruction := vm.memory[vm.ip]
	opCode := instruction % 100
	p1Mode := (instruction / 100) % 10
	p2Mode := (instruction / 1000) % 10
	switch opCode {
	case 1:
		{
			vm.memory[vm.param(3, 1)] = vm.param(1, p1Mode) + vm.param(2, p2Mode)
			vm.ip += 4
			break
		}
	case 2:
		{
			vm.memory[vm.param(3, 1)] = vm.param(1, p1Mode) * vm.param(2, p2Mode)
			vm.ip += 4
			break
		}
	case 3:
		{
			vm.memory[vm.param(1, 1)] = vm.input
			vm.ip += 2
			break
		}
	case 4:
		{
			vm.input = vm.memory[vm.param(1, 1)]
			fmt.Println("Output ", vm.input)
			vm.ip += 2
			break
		}
	case 5:
		{
			if vm.param(1, p1Mode) != 0 {
				vm.ip = vm.param(2, p2Mode)
			} else {
				vm.ip += 3
			}
			break
		}
	case 6:
		{
			if vm.param(1, p1Mode) == 0 {
				vm.ip = vm.param(2, p2Mode)
			} else {
				vm.ip += 3
			}
			break
		}
	case 7:
		{
			if vm.param(1, p1Mode) < vm.param(2, p2Mode) {
				vm.memory[vm.param(3, 1)] = 1
			} else {
				vm.memory[vm.param(3, 1)] = 0
			}
			vm.ip += 4
			break
		}
	case 8:
		{
			if vm.param(1, p1Mode) == vm.param(2, p2Mode) {
				vm.memory[vm.param(3, 1)] = 1
			} else {
				vm.memory[vm.param(3, 1)] = 0
			}
			vm.ip += 4
			break
		}
	case 99:
		{
			vm.terminated = true
			break
		}
	}
}

func compute1(input string) {
	vm := createVM(parseIntArray(input), 1)
	for {
		vm.step()
		if vm.terminated {
			break
		}
	}
}

func compute2(input string) {
	vm := createVM(parseIntArray(input), 5)
	for {
		vm.step()
		if vm.terminated {
			break
		}
	}
}

func main() {
	compute1(load("../inputs/05.txt"))
	compute2(load("../inputs/05.txt"))
}
