package main

import (
	"fmt"
	"strconv"
	"strings"
)

type ctx17 struct {
	lines []string
}

func (ctx *ctx17) dump() {
	for _, line := range ctx.lines {
		fmt.Println(line)
	}
}

func (ctx *ctx17) findIntersections() int64 {
	w := len(ctx.lines[0])
	h := len(ctx.lines)
	count := int64(0)
	for y := 1; y < h-1; y++ {
		for x := 1; x < w-1; x++ {
			if ctx.lines[y][x] == '#' && ctx.lines[y-1][x] == '#' && ctx.lines[y][x-1] == '#' && ctx.lines[y+1][x] == '#' && ctx.lines[y][x+1] == '#' {
				count += int64(x * y)
			}
		}
	}
	return count
}

func (ctx *ctx17) computePath() {
	w := int64(len(ctx.lines[0]))
	h := int64(len(ctx.lines))
	// find robot
	pos := vec2i64{0, 0}
	dir := vec2i64{0, -1}
	for y := int64(0); y < h; y++ {
		for x := int64(0); x < w; x++ {
			if ctx.lines[y][x] == '^' {
				pos = vec2i64{x, y}
			}
		}
	}
	fmt.Println("Robot at ", pos, dir)
	// Compute path
	path := []string{}
	for {
		// Check where the path continues
		leftDir := dir.rotLeft()
		leftPos := pos.add(leftDir)
		rightDir := dir.rotRight()
		rightPos := pos.add(rightDir)
		if leftPos.inBounds(0, 0, w-1, h-1) && ctx.lines[leftPos.y][leftPos.x] == '#' {
			dir = leftDir
			path = append(path, "L")
		} else if rightPos.inBounds(0, 0, w-1, h-1) && ctx.lines[rightPos.y][rightPos.x] == '#' {
			dir = rightDir
			path = append(path, "R")
		} else {
			fmt.Println("Computed path: ", strings.Join(path, ","))
			return
		}
		// Count distance
		distance := 0
		for {
			nextPos := pos.add(dir)
			if nextPos.inBounds(0, 0, w-1, h-1) && ctx.lines[nextPos.y][nextPos.x] == '#' {
				distance++
				pos = nextPos
			} else {
				path = append(path, strconv.Itoa(distance))
				break
			}
		}
	}
}

func computeMap(input string) *ctx17 {
	vm := createIntCodeVM(parseInt64Array(input))
	go vm.run()
	ctx := &ctx17{[]string{}}
	line := ""
	for {
		select {
		case x := <-vm.output:
			if x == 10 {
				if len(line) > 0 {
					ctx.lines = append(ctx.lines, line)
				}
				line = ""
			} else {
				line += string(rune(x))
			}
			break
		case <-vm.done:
			ctx.dump()
			return ctx
		}
	}
}

func compute17a(input string) int64 {
	ctx := computeMap(input)
	return ctx.findIntersections()
}

func compute17b(input string) int64 {
	ctx := computeMap(input)
	ctx.computePath()
	// computePath is not actually used, I cheated and created the output by hand
	program := []string{
		"A,B,A,B,A,C,B,C,A,C",
		"L,6,R,12,L,6",
		"R,12,L,10,L,4,L,6",
		"L,10,L,10,L,4,L,6",
	}
	// Input the path
	mem := parseInt64Array(input)
	mem[0] = 2
	vm := createIntCodeVM(mem)
	go vm.run()
	// consume the entire input image
	wasNewline := false
	for {
		c := <-vm.output
		if c == 10 {
			if wasNewline {
				break
			}
			wasNewline = true
		} else {
			wasNewline = false
		}
	}
	// input instructions
	for _, line := range program {
		for {
			c := <-vm.output
			if c == 10 {
				break
			}
		}
		for _, c := range line {
			vm.input <- int64(c)
		}
		vm.input <- int64(10)
	}
	for {
		c := <-vm.output
		if c == 10 {
			break
		}
	}
	vm.input <- int64('n')
	vm.input <- int64(10)
	lastOutput := int64(0)
	line := ""
	for {
		select {
		case c := <-vm.output:
			{
				if c == 10 {
					if len(line) > 0 {
						fmt.Println(line)
					}
					line = ""
				} else {
					line += string(rune(c))
				}
				lastOutput = c
				break
			}
		case <-vm.done:
			{
				return lastOutput
			}
		}
	}
}

func main() {
	fmt.Println("Result A", compute17a(load("../inputs/17.txt")))
	fmt.Println("Result B", compute17b(load("../inputs/17.txt")))
}
