package main

import (
	"fmt"
)

const none = -1
const up = 0
const down = 1
const left = 2
const right = 3

func parse24(input string) int {
	value := 0
	index := uint(0)
	for _, c := range input {
		if c == '\n' {
			continue
		}
		if c == '#' {
			value |= 1 << index
		}
		index++
	}
	return value
}

func iterate24(value int) int {
	newValue := 0
	for y := 0; y < 5; y++ {
		for x := 0; x < 5; x++ {
			bit := 1 << uint(x+y*5)
			sum := 0
			if x > 0 && (value&(1<<uint(x+y*5-1))) > 0 {
				sum++
			}
			if x < 4 && (value&(1<<uint(x+y*5+1))) > 0 {
				sum++
			}
			if y > 0 && (value&(1<<uint(x+y*5-5))) > 0 {
				sum++
			}
			if y < 4 && (value&(1<<uint(x+y*5+5))) > 0 {
				sum++
			}
			if value&bit > 0 {
				// A bug dies (becoming an empty space) unless there is exactly one bug
				// adjacent to it.
				if sum == 1 {
					newValue |= bit
				}
			} else {
				// An empty space becomes infested with a bug if exactly one or two bugs
				// are adjacent to it.
				if sum == 1 || sum == 2 {
					newValue |= bit
				}
			}
		}
	}
	return newValue
}

func dump24(value int) {
	for y := 0; y < 5; y++ {
		for x := 0; x < 5; x++ {
			bit := 1 << uint(x+y*5)
			if value&bit != 0 {
				fmt.Print("#")
			} else {
				fmt.Print(".")
			}
		}
		fmt.Println("")
	}
}

func compute24a(input string) int {
	value := parse24(input)
	// dump24(value)
	seen := make(map[int]bool)
	seen[value] = true
	for it := 0; ; it++ {
		// fmt.Println(it)
		value = iterate24(value)
		// dump24(value)
		_, done := seen[value]
		if done {
			return value
		}
		seen[value] = true
	}
	return value
}

type ctx24 struct {
	layers   map[int]int
	minLayer int
	maxLayer int
}

func (ctx *ctx24) count() int {
	sum := 0
	for _, layer := range ctx.layers {
		for i := uint(0); i < 25; i++ {
			if layer&(1<<i) != 0 {
				sum++
			}
		}
	}
	return sum
}

func (ctx *ctx24) value(x, y int, layer int, dir int) int {
	if x == 2 && y == 2 {
		// Doesn't apply
		panic("Should query the center")
		return 0
	}
	if dir == none {
		if ctx.layers[layer]&(1<<uint(x+y*5)) > 0 {
			return 1
		}
		return 0
	}
	if dir == up {
		if x == 2 && y == 3 {
			// Bottom row of layer down
			return ctx.value(0, 4, layer-1, none) + ctx.value(1, 4, layer-1, none) + ctx.value(2, 4, layer-1, none) + ctx.value(3, 4, layer-1, none) + ctx.value(4, 4, layer-1, none)
		}
		if y == 0 {
			// top of center of layer up
			return ctx.value(2, 1, layer+1, none)
		}
		// Else it's just a normal sample within this layer
		return ctx.value(x, y-1, layer, none)
	}
	if dir == down {
		if x == 2 && y == 1 {
			// Top row of layer down
			return ctx.value(0, 0, layer-1, none) + ctx.value(1, 0, layer-1, none) + ctx.value(2, 0, layer-1, none) + ctx.value(3, 0, layer-1, none) + ctx.value(4, 0, layer-1, none)
		}
		if y == 4 {
			// bottom of center of layer up
			return ctx.value(2, 3, layer+1, none)
		}
		// Else it's just a normal sample within this layer
		return ctx.value(x, y+1, layer, none)
	}
	if dir == left {
		if x == 3 && y == 2 {
			// Right column of layer down
			return ctx.value(4, 0, layer-1, none) + ctx.value(4, 1, layer-1, none) + ctx.value(4, 2, layer-1, none) + ctx.value(4, 3, layer-1, none) + ctx.value(4, 4, layer-1, none)
		}
		if x == 0 {
			// left of center of layer up
			return ctx.value(1, 2, layer+1, none)
		}
		// Else it's just a normal sample within this layer
		return ctx.value(x-1, y, layer, none)
	}
	if dir == right {
		if x == 1 && y == 2 {
			// Left column of layer down
			return ctx.value(0, 0, layer-1, none) + ctx.value(0, 1, layer-1, none) + ctx.value(0, 2, layer-1, none) + ctx.value(0, 3, layer-1, none) + ctx.value(0, 4, layer-1, none)
		}
		if x == 4 {
			// right of center of layer up
			return ctx.value(3, 2, layer+1, none)
		}
		// Else it's just a normal sample within this layer
		return ctx.value(x+1, y, layer, none)
	}
	panic("Shouldn't happen")
	return 0
}

func (ctx *ctx24) iterate() {
	ctx.minLayer--
	ctx.maxLayer++
	newLayers := make(map[int]int)
	for l := ctx.minLayer; l <= ctx.maxLayer; l++ {
		newValue := 0
		for y := 0; y < 5; y++ {
			for x := 0; x < 5; x++ {
				if x == 2 && y == 2 {
					// Skip center
					continue
				}
				sum := 0
				sum += ctx.value(x, y, l, up)
				sum += ctx.value(x, y, l, down)
				sum += ctx.value(x, y, l, left)
				sum += ctx.value(x, y, l, right)
				if ctx.value(x, y, l, none) > 0 {
					// A bug dies (becoming an empty space) unless there is exactly one bug
					// adjacent to it.
					if sum == 1 {
						newValue |= 1 << uint(x+y*5)
					}
				} else {
					// An empty space becomes infested with a bug if exactly one or two bugs
					// are adjacent to it.
					if sum == 1 || sum == 2 {
						newValue |= 1 << uint(x+y*5)
					}
				}
			}
		}
		// fmt.Println(l)
		// dump24(newValue)
		newLayers[l] = newValue
	}
	ctx.layers = newLayers
	// Contract layers
	for ctx.minLayer < 0 && ctx.layers[ctx.minLayer] == 0 {
		ctx.minLayer++
	}
	for ctx.maxLayer > 0 && ctx.layers[ctx.maxLayer] == 0 {
		ctx.maxLayer--
	}
}

func compute24b(input string, minutes int) int {
	value := parse24(input)
	ctx := ctx24{
		layers: map[int]int{0: value},
	}
	for i := 0; i < minutes; i++ {
		ctx.iterate()
	}
	return ctx.count()
}

func test24b(input string) int {
	return compute24b(input, 10)
}

func main() {
	test(parse24, ".....\n.....\n.....\n#....\n.#...", 2129920)
	test(compute24a, "....#\n#..#.\n#..##\n..#..\n#....", 2129920)
	fmt.Println("Result A", compute24a(load("../inputs/24.txt")))
	test(test24b, "....#\n#..#.\n#.?##\n..#..\n#....", 99)
	fmt.Println("Result B", compute24b(load("../inputs/24.txt"), 200))
}
