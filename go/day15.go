package main

import (
	"container/heap"
	"fmt"
)

type xy15 struct {
	x int64
	y int64
}

func (xy *xy15) dist1(dst xy15) int64 {
	return absInt64(xy.x-dst.x) + absInt64(xy.y-dst.y)
}

type ctx15 struct {
	vm     *intCodeVM
	xy     xy15
	target *xy15
	grid   map[xy15]int64
	queue  []xy15
}

const unknown = 0
const wall = 1
const walkable = 2

const north = 1
const south = 2
const west = 3
const east = 4

type pqNode15 struct {
	xy     xy15
	fScore int64 // total distance via this node (est)
	// index  int
}

type prioQueue15 []*pqNode15

func (pq prioQueue15) Len() int {
	return len(pq)
}

func (pq prioQueue15) Less(i, j int) bool {
	return pq[i].fScore < pq[j].fScore
}

func (pq prioQueue15) Swap(i, j int) {
	pq[i], pq[j] = pq[j], pq[i]
	// pq[i].index = i
	// pq[j].index = j
}

func (pq *prioQueue15) Push(x interface{}) {
	// n := len(*pq)
	no := x.(*pqNode15)
	// no.index = n
	*pq = append(*pq, no)
}

func (pq *prioQueue15) Pop() interface{} {
	old := *pq
	n := len(old)
	no := old[n-1]
	// no.index = -1
	*pq = old[0 : n-1]
	return no
}

func (pq pqNode15) String() string {
	return fmt.Sprintf("%d,%d @ %d", pq.xy.x, pq.xy.y, pq.fScore)
}

func (pq prioQueue15) String() string {
	s := ""
	for _, node := range pq {
		s += node.String() + ";"
	}
	return s
}

type node15 struct {
	pos       xy15
	source    xy15
	sourceDir int64
	gScore    int64
	fScore    int64
}

func (ctx *ctx15) pathFromTo(source xy15, target xy15) []int64 {
	// fmt.Println("Path to ", target)
	nodes := make(map[xy15]*node15)
	nodes[source] = &node15{
		pos:       source,
		source:    source,
		gScore:    0,
		sourceDir: 0,
		fScore:    source.dist1(target),
	}
	// Add a priority queue for next path elements
	queue := make(prioQueue15, 0)
	heap.Push(&queue, &pqNode15{
		xy:     source,
		fScore: nodes[source].fScore,
	})
	// Get item from PQ, check distance, and add neighbors to queue
	for queue.Len() > 0 {
		// fmt.Println("pq", queue)
		next := heap.Pop(&queue).(*pqNode15)
		nextNode := nodes[next.xy]
		// fmt.Println("pqNext", next, nextNode)
		if next.xy == target {
			// Found the target
			// fmt.Println("Found target")
			backNode := nextNode
			path := []int64{}
			for {
				if backNode.sourceDir == 0 {
					return path
				}
				dir := backNode.sourceDir
				backNode = nodes[backNode.source]
				path = append([]int64{dir}, path...)
			}
			break
		}
		// Add neighbors
		for dir := int64(1); dir < 5; dir++ {
			newXY := next.xy.copyDir(dir)
			// Check if this is walkable
			if ctx.grid[newXY] != walkable && newXY != target {
				continue
			}
			gScore := nextNode.gScore + 1
			newNode := nodes[newXY]
			if newNode == nil {
				// New node, so create. Trivially is the closest parent
				newNode = &node15{
					pos:       newXY,
					source:    next.xy,
					sourceDir: dir,
					gScore:    gScore,
					fScore:    gScore + newXY.dist1(target),
				}
				nodes[newXY] = newNode
				heap.Push(&queue, &pqNode15{
					xy:     newXY,
					fScore: newNode.fScore,
				})
			} else {
				if gScore < newNode.gScore {
					// We're closer, so update the node
					newNode.gScore = gScore
					newNode.fScore = gScore + newXY.dist1(target)
					newNode.source = next.xy
					newNode.sourceDir = dir
				}
			}
		}
	}
	// TODO(VS) build the path
	fmt.Println("EEE No path found!")
	return []int64{}
}

func (ctx *ctx15) queued(xy xy15) bool {
	for _, value := range ctx.queue {
		if value == xy {
			return true
		}
	}
	return false
}

func (ctx *ctx15) dump() {
	// Find min/max coords
	min := xy15{99999999, 99999999}
	max := xy15{-99999999, -99999999}
	for cell := range ctx.grid {
		min.x = min64(min.x, cell.x)
		min.y = min64(min.y, cell.y)
		max.x = max64(max.x, cell.x)
		max.y = max64(max.y, cell.y)
	}
	for y := max.y + 1; y >= min.y-1; y-- {
		line := ""
		for x := min.x - 1; x <= max.x+1; x++ {
			val := ctx.grid[xy15{x, y}]
			if x == ctx.target.x && y == ctx.target.y {
				line += "X"
			} else if x == ctx.xy.x && y == ctx.xy.y {
				line += "@"
			} else if val == walkable {
				line += "."
			} else if val == wall {
				line += "#"
			} else {
				if ctx.queued(xy15{x, y}) {
					line += "?"
				} else {
					line += " "
				}
			}
		}
		fmt.Println(line)
	}
}

func (ctx *ctx15) queueIfUnknown(dir int64) {
	pos := ctx.xy.copyDir(dir)
	if ctx.grid[pos] == unknown {
		ctx.queue = append([]xy15{pos}, ctx.queue...)
	}
}

func (ctx *ctx15) step() bool {
	if len(ctx.queue) == 0 {
		return false
	}
	// Take next target from queue
	next := ctx.queue[0]
	ctx.queue = ctx.queue[1:]
	// Check if item on queue is unknown
	if ctx.grid[next] != unknown {
		// Already known, ignore
		return true
	}
	// Move towards target
	path := ctx.pathFromTo(ctx.xy, next)
	// fmt.Println("Next", next, "Path", path)
	for _, input := range path {
		ctx.vm.input <- input
		value := <-ctx.vm.output
		if value == 0 {
			// wall
			ctx.grid[ctx.xy.copyDir(input)] = wall
		} else {
			ctx.xy.applyDir(input)
			ctx.grid[ctx.xy] = walkable
			if value == 2 {
				ctx.target = &xy15{ctx.xy.x, ctx.xy.y}
			}
			// add to queue
			ctx.queueIfUnknown(north)
			ctx.queueIfUnknown(south)
			ctx.queueIfUnknown(east)
			ctx.queueIfUnknown(west)
		}
	}
	return true
}

func (xy *xy15) applyDir(dir int64) {
	switch dir {
	case north:
		{
			xy.y++
			break
		}
	case south:
		{
			xy.y--
			break
		}
	case east:
		{
			xy.x++
			break
		}
	case west:
		{
			xy.x--
			break
		}
	}
}

func (xy *xy15) copyDir(dir int64) xy15 {
	xy2 := xy15{xy.x, xy.y}
	xy2.applyDir(dir)
	return xy2
}

func compute15a(input string) int64 {
	ctx := new(ctx15)
	ctx.vm = createIntCodeVM(parseInt64Array(input))
	go ctx.vm.run()
	ctx.grid = make(map[xy15]int64)
	ctx.grid[ctx.xy] = walkable
	ctx.queue = []xy15{
		ctx.xy.copyDir(north),
		ctx.xy.copyDir(south),
		ctx.xy.copyDir(east),
		ctx.xy.copyDir(west),
	}
	for {
		if ctx.step() == false {
			ctx.dump()
			path := ctx.pathFromTo(xy15{0, 0}, *ctx.target)
			return int64(len(path))
		}
	}
	return 0
}

func compute15b(input string) int64 {
	ctx := new(ctx15)
	ctx.vm = createIntCodeVM(parseInt64Array(input))
	go ctx.vm.run()
	ctx.grid = make(map[xy15]int64)
	ctx.grid[ctx.xy] = walkable
	ctx.queue = []xy15{
		ctx.xy.copyDir(north),
		ctx.xy.copyDir(south),
		ctx.xy.copyDir(east),
		ctx.xy.copyDir(west),
	}
	for {
		if ctx.step() == false {
			// Now find the longest path
			// This is brute force, but it's fine because our A* is fast
			maxDist := int64(0)
			for xy, kind := range ctx.grid {
				if kind != walkable {
					continue
				}
				dist := len(ctx.pathFromTo(*ctx.target, xy))
				maxDist = max64(maxDist, int64(dist))
			}
			return maxDist
		}
	}
	return 0
}

func main() {
	fmt.Println("Result A", compute15a(load("../inputs/15.txt")))
	fmt.Println("Result B", compute15b(load("../inputs/15.txt")))
}
