package main

import (
	"fmt"
	"strconv"
	"strings"
)

type cell struct {
	flags       int
	x           int
	y           int
	sumDistance int
}

type context struct {
	grid map[string]*cell
}

func parse(str string) [][]string {
	sarray := strings.Split(str, "\n")
	wires := make([][]string, len(sarray))
	for idx, s := range sarray {
		wires[idx] = strings.Split(s, ",")
	}
	return wires
}

func (ctx *context) trace(wire []string, wireid int) {
	x, y := 0, 0
	distance := 0
	for _, segment := range wire {
		dx, dy := 0, 0
		switch segment[0] {
		case 'U':
			dy = 1
		case 'D':
			dy = -1
		case 'L':
			dx = -1
		case 'R':
			dx = 1
		}
		len, _ := strconv.Atoi(segment[1:])
		for i := 0; i < len; i++ {
			x += dx
			y += dy
			distance++
			idx := fmt.Sprint(x, ",", y)
			if _, ok := ctx.grid[idx]; !ok {
				cell := new(cell)
				cell.x = x
				cell.y = y
				ctx.grid[idx] = cell
			}
			if ctx.grid[idx].flags&wireid == 0 {
				ctx.grid[idx].flags |= wireid
				ctx.grid[idx].sumDistance += distance
			}
		}
	}
}

func (ctx *context) traceWires(wires [][]string) {
	ctx.grid = make(map[string]*cell, 0)
	for idx, wire := range wires {
		ctx.trace(wire, (1 << uint(idx)))
	}
}

func compute1(input string) int {
	wires := parse(input)
	ctx := new(context)
	ctx.traceWires(wires)
	mind := 999999999999
	for _, cell := range ctx.grid {
		if cell.flags == 3 {
			d := absInt(cell.x) + absInt(cell.y)
			if d < mind {
				mind = d
			}
		}
	}
	return mind
}

func compute2(input string) int {
	wires := parse(input)
	ctx := new(context)
	ctx.traceWires(wires)
	mind := 999999999999
	for _, cell := range ctx.grid {
		if cell.flags == 3 {
			if cell.sumDistance < mind {
				mind = cell.sumDistance
			}
		}
	}
	return mind
}

func main() {
	test(compute1, "R8,U5,L5,D3\nU7,R6,D4,L4", 6)
	test(compute1, "R75,D30,R83,U83,L12,D49,R71,U7,L72\nU62,R66,U55,R34,D71,R55,D58,R83", 159)
	test(compute1, "R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51\nU98,R91,D20,R16,D67,R40,U7,R15,U6,R7", 135)
	fmt.Println(compute1(load("../inputs/03.txt")))
	test(compute2, "R8,U5,L5,D3\nU7,R6,D4,L4", 30)
	test(compute2, "R75,D30,R83,U83,L12,D49,R71,U7,L72\nU62,R66,U55,R34,D71,R55,D58,R83", 610)
	test(compute2, "R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51\nU98,R91,D20,R16,D67,R40,U7,R15,U6,R7", 410)
	fmt.Println(compute2(load("../inputs/03.txt")))
}
