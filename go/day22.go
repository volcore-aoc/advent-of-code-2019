package main

import (
	"fmt"
	"math/big"
	"strings"
)

type instruction22 struct {
	kind  string
	value int64
}

type ctx22 struct {
	instructions []instruction22
}

func parse22(input string) *ctx22 {
	lines := strings.Split(strings.Trim(input, "\n\r \t"), "\n")
	instructions := []instruction22{}
	for _, line := range lines {
		inst := instruction22{}
		if strings.HasPrefix(line, "deal with increment") {
			inst.kind = "inc"
			inst.value = ParseS64(line[20:])
		} else if strings.HasPrefix(line, "deal into new stack") {
			inst.kind = "deal"
		} else if strings.HasPrefix(line, "cut") {
			inst.kind = "cut"
			inst.value = ParseS64(line[4:])
		}
		instructions = append(instructions, inst)
	}
	return &ctx22{instructions}
}

func (ctx *ctx22) runa(count int64) []int64 {
	values := make([]int64, count)
	tmp := make([]int64, count)
	for i := range values {
		values[i] = int64(i)
	}
	for _, inst := range ctx.instructions {
		switch inst.kind {
		case "inc":
			{
				for i := int64(0); i < count; i++ {
					j := (i * inst.value) % int64(len(values))
					tmp[j] = values[i]
				}
				tmp, values = values, tmp
				break
			}
		case "deal":
			{
				for i := int64(0); i < count; i++ {
					tmp[i] = values[int64(len(values))-i-1]
				}
				tmp, values = values, tmp
				break
			}
		case "cut":
			{
				split := inst.value
				if inst.value < 0 {
					split = int64(len(values)) + inst.value
				}
				values = append(values[split:], values[:split]...)
				break
			}
		}
		// fmt.Println(values)
	}
	return values
}

func (ctx *ctx22) computeAB(count int64) (int64, int64) {
	a := int64(1)
	b := int64(0)
	for _, inst := range ctx.instructions {
		switch inst.kind {
		case "inc":
			{
				// ax+b => v(ax+b) = vax+vb
				a *= inst.value
				b *= inst.value
				break
			}
		case "deal":
			{
				// ax+b => -ax-b-1
				a = -a
				b = -b - 1
				break
			}
		case "cut":
			{
				// ax+b => ax+b-v
				b = b - inst.value
				break
			}
		}
		a = PositiveMod(a, count)
		b = PositiveMod(b, count)
	}
	return a, b
}

func test22a(input string) int64 {
	ctx := parse22(input)
	values := ctx.runa(10)
	res := int64(0)
	for i := 0; i < len(values); i++ {
		res *= 10
		res += values[i]
	}
	return res
}

func compute22a(input string) int64 {
	ctx := parse22(input)
	values := ctx.runa(10007)
	for i := range values {
		if values[i] == 2019 {
			return int64(i)
		}
	}
	return 0
}

func compute22b(input string) int64 {
	ctx := parse22(input)
	count := int64(119315717514047)
	shuffles := int64(101741582076661)
	// compute the forward LCG
	a, b := ctx.computeAB(count)
	// Constants
	A := big.NewInt(a)              // a
	B := big.NewInt(b)              // b
	invA := big.NewInt(1 - a)       // 1 - a
	n := big.NewInt(shuffles)       // n
	m := big.NewInt(count)          // m
	m2 := big.NewInt(count - 2)     // m - 2
	nm2 := big.NewInt(0).Mul(n, m2) // n * (m - 2)
	pos := big.NewInt(2020)
	// Compute the inverse r
	// r = (b * pow((1-a), m - 2, m)) % m
	powInvA := big.NewInt(0).Exp(invA, m2, m) // pow((1-a), m - 2, m)
	rp := big.NewInt(0).Mul(B, powInvA)       // b * pow((1-a), m - 2, m)
	r := big.NewInt(0).Mod(rp, m)             // (b * pow((1-a), m - 2, m)) % m
	// Compute the card
	// card = ((pos - r) * pow(a, n*(m-2), m) + r) % m
	posR := big.NewInt(0).Sub(pos, r)           // pos - r
	powA := big.NewInt(0).Exp(A, nm2, m)        // pow(a, n*(m-2), m)
	posRPowA := big.NewInt(0).Mul(posR, powA)   // (pos - r) * pow(a, n*(m-2), m)
	cardNoMod := big.NewInt(0).Add(posRPowA, r) // (pos - r) * pow(a, n*(m-2), m) + r
	card := big.NewInt(0).Mod(cardNoMod, m)     // ((pos - r) * pow(a, n*(m-2), m) + r) %m
	return card.Int64()
}

func main() {
	test64(test22a, "deal with increment 7\ndeal into new stack\ndeal into new stack", 369258147)
	test64(test22a, "cut 6\ndeal with increment 7\ndeal into new stack", 3074185296)
	test64(test22a, "deal with increment 7\ndeal with increment 9\ncut -2", 6307418529)
	test64(test22a, "deal into new stack\ncut -2\ndeal with increment 7\ncut 8\ncut -4\ndeal with increment 7\ncut 3\ndeal with increment 9\ndeal with increment 3\ncut -1", 9258147036)
	fmt.Println("Result A", compute22a(load("../inputs/22.txt")))
	fmt.Println("Result B", compute22b(load("../inputs/22.txt")))
}
