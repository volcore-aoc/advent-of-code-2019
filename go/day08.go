package main

import (
	"fmt"
	"strings"
)

func compute08a(input string, w int, h int) int {
	idx := 0
	min0 := w * h
	maxValue := 0
	for {
		if idx >= len(input) {
			return maxValue
		}
		hist := make([]int, 3)
		for i := 0; i < w*h; i++ {
			hist[int(input[idx]-'0')]++
			idx++
		}
		if hist[0] < min0 {
			min0 = hist[0]
			maxValue = hist[1] * hist[2]
		}
	}
}

func compute08b(input string, w int, h int) string {
	stride := w * h
	layers := len(input) / stride
	var image strings.Builder
	for y := 0; y < h; y++ {
		for x := 0; x < w; x++ {
			off := x + y*w
			color := input[off]
			for l := 1; l < layers && color == 50; l++ {
				color = input[l*stride+off]
			}
			if color == '0' {
				color = ' '
			}
			image.WriteByte(color)
		}
	}
	img := image.String()
	for y := 0; y < h; y++ {
		fmt.Println(img[y*w : y*w+w])
	}
	return img
}

func main() {
	fmt.Println(compute08a(load("../inputs/08.txt"), 25, 6))
	testS(func(in string) string { return compute08b(in, 2, 2) }, "0222112222120000", " 11 ")
	compute08b(load("../inputs/08.txt"), 25, 6)
}
