package main

import (
	"fmt"
	"strconv"
	"strings"
)

func parse(str string) []int {
	sarray := strings.Split(str, "\n")
	iarray := make([]int, len(sarray))
	for idx, s := range sarray {
		value, err := strconv.Atoi(s)
		iarray[idx] = value
		if err != nil {
			panic("Failed to parse int " + s)
		}
	}
	return iarray
}

func eval1(x int) int {
	return x/3 - 2
}

func compute1(input string) int {
	list := parse(input)
	sum := 0
	for _, x := range list {
		sum += eval1(x)
	}
	return sum
}

func main() {
	test(compute1, "12", 2)
	test(compute1, "12\n14\n1969\n100756", 2+2+654+33583)
	fmt.Println(compute1(load("../inputs/01.txt")))
}
