package main

import (
	"fmt"
	"strconv"
	"strings"
)

var basePattern = [...]int{0, 1, 0, -1}

func parse16(input string) []int {
	input = strings.Trim(input, " \n\r")
	list := make([]int, len(input))
	for i, c := range input {
		list[i] = int(c - '0')
	}
	return list
}

func unparse(list []int, count int, offset int) string {
	res := ""
	for idx := offset; idx < offset+count; idx++ {
		if len(list) <= idx {
			res += "0"
		} else {
			res += strconv.Itoa(list[idx])
		}
	}
	return res
}

func computePhase(current []int, next []int) {
	for j := range next {
		val := 0
		for i := range current {
			pidx := ((i + 1) / (j + 1)) % len(basePattern)
			val += basePattern[pidx] * current[i]
		}
		val = val % 10
		if val < 0 {
			val = -val
		}
		next[j] = val
	}
}

func compute16a(input string, phases int) string {
	current := parse16(input)
	next := make([]int, len(current))
	for phase := 0; phase < phases; phase++ {
		computePhase(current, next)
		current, next = next, current
	}
	return unparse(current, 8, 0)
}

func compute16b(input string, phases int) string {
	input = strings.Repeat(strings.TrimSpace(input), 10000)
	current := parse16(input)
	offset, _ := strconv.Atoi(input[:7])
	current = current[offset:]
	next := make([]int, len(current))
	for phase := 0; phase < phases; phase++ {
		// Assume that offset  > half of the string, so the values are just a
		// backwards sum
		sum := 0
		for i := len(current) - 1; i >= 0; i-- {
			sum += current[i]
			next[i] = sum % 10
		}
		current, next = next, current
	}
	return unparse(current, 8, 0)
}

func test16a(phases int) func(string) string {
	return func(input string) string { return compute16a(input, phases) }
}

func test16b(phases int) func(string) string {
	return func(input string) string { return compute16b(input, phases) }
}

func main() {
	testS(test16a(1), "12345678", "48226158")
	testS(test16a(2), "12345678", "34040438")
	testS(test16a(3), "12345678", "03415518")
	testS(test16a(4), "12345678", "01029498")
	testS(test16a(100), "80871224585914546619083218645595", "24176176")
	testS(test16a(100), "19617804207202209144916044189917", "73745418")
	testS(test16a(100), "69317163492948606335995924319873", "52432133")
	fmt.Println("Result A", compute16a(load("../inputs/16.txt"), 100))
	testS(test16b(100), "03036732577212944063491565474664", "84462026")
	testS(test16b(100), "02935109699940807407585447034323", "78725270")
	testS(test16b(100), "03081770884921959731165446850517", "53553731")
	fmt.Println("Result B", compute16b(load("../inputs/16.txt"), 100))
}
