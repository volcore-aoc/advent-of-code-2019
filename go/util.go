package main

import (
	"fmt"
	"io/ioutil"
	"math"
	"sort"
	"strconv"
	"strings"
)

func load(path string) string {
	b, err := ioutil.ReadFile(path)
	if err != nil {
		panic("Unable to find file " + path)
	}
	return strings.Trim(string(b), "\n\r \t")
}

func loadNoTrim(path string) string {
	b, err := ioutil.ReadFile(path)
	if err != nil {
		panic("Unable to find file " + path)
	}
	return string(b)
}

func parseIntArray(str string) []int {
	list := strings.Split(str, ",")
	ilist := make([]int, len(list))
	for i := 0; i < len(list); i++ {
		v, _ := strconv.ParseInt(list[i], 10, 32)
		ilist[i] = int(v)
	}
	return ilist
}

func parseInt64Array(str string) []int64 {
	list := strings.Split(str, ",")
	ilist := make([]int64, len(list))
	for i := 0; i < len(list); i++ {
		ilist[i], _ = strconv.ParseInt(list[i], 10, 64)
	}
	return ilist
}

func PositiveMod(i int64, n int64) int64 {
	return ((i % n) + n) % n
}

func ParseS64(s string) int64 {
	v, _ := strconv.ParseInt(s, 10, 64)
	return v
}

func max(x int, y int) int {
	if x < y {
		return y
	} else {
		return x
	}
}

func max64(x int64, y int64) int64 {
	if x < y {
		return y
	} else {
		return x
	}
}

func min(x int, y int) int {
	if x > y {
		return y
	} else {
		return x
	}
}

func min64(x int64, y int64) int64 {
	if x > y {
		return y
	} else {
		return x
	}
}

func sign(x int) int {
	if x < 0 {
		return -1
	} else if x == 0 {
		return 0
	}
	return 1
}

func absInt(x int) int {
	if x < 0 {
		return -x
	}
	return x
}

func absInt64(x int64) int64 {
	if x < 0 {
		return -x
	}
	return x
}

func normal(ox int, oy int) (float64, float64) {
	len := math.Sqrt(float64(ox*ox) + float64(oy*oy))
	fx := float64(ox) / len
	fy := float64(oy) / len
	return fx, fy
}

func computeAngle(ox int, oy int) float64 {
	return math.Atan2(float64(oy), float64(ox))
}

func printResult(input string, output string) {
	input = strings.Replace(input, "\n", " ", -1)
	if len(input) > 50 {
		input = input[:50] + "..."
	}
	fmt.Println(input, "=>", output)
}

func test(f func(string) int, in string, out int) {
	r := f(in)
	printResult(in, strconv.Itoa(r))
	if r != out {
		fmt.Println("Test failed: ", r, " should be ", out)
	}
}

func test64(f func(string) int64, in string, out int64) {
	r := f(in)
	printResult(in, strconv.FormatInt(r, 10))
	if r != out {
		fmt.Println("Test failed: ", r, " should be ", out)
	}
}

func testS(f func(string) string, in string, out string) {
	r := f(in)
	printResult(in, r)
	if r != out {
		fmt.Println("Test failed: ", r, " should be ", out)
	}
}

func gcd(a int64, b int64) int64 {
	for b != 0 {
		t := b
		b = a % b
		a = t
	}
	return a
}

func lcm(a int64, b int64, rest ...int64) int64 {
	result := a * b / gcd(a, b)
	for i := 0; i < len(rest); i++ {
		result = lcm(result, rest[i])
	}
	return result
}

type vec2i struct {
	x int
	y int
}

func (xy *vec2i) dist1(dst vec2i) int {
	return absInt(xy.x-dst.x) + absInt(xy.y-dst.y)
}

func (xy *vec2i) add(o vec2i) vec2i {
	return vec2i{xy.x + o.x, xy.y + o.y}
}

func (xy *vec2i) rotLeft() vec2i {
	return vec2i{xy.y, -xy.x}
}

func (xy *vec2i) rotRight() vec2i {
	return vec2i{-xy.y, xy.x}
}

func (xy *vec2i) inBounds(minx int, miny int, maxx int, maxy int) bool {
	return xy.x >= minx && xy.x <= maxx && xy.y >= miny && xy.y <= maxy
}

type vec3i struct {
	x int
	y int
	z int
}

func (xyz *vec3i) dist1(dst vec3i) int {
	return absInt(xyz.x-dst.x) + absInt(xyz.y-dst.y) + absInt(xyz.z-dst.z)
}

func (xyz *vec3i) add(o vec3i) vec3i {
	return vec3i{xyz.x + o.x, xyz.y + o.y, xyz.z + o.z}
}

func (xyz *vec3i) xy() vec2i {
	return vec2i{xyz.x, xyz.y}
}

type vec2i64 struct {
	x int64
	y int64
}

func (xy *vec2i64) dist1(dst vec2i64) int64 {
	return absInt64(xy.x-dst.x) + absInt64(xy.y-dst.y)
}

func (xy *vec2i64) add(o vec2i64) vec2i64 {
	return vec2i64{xy.x + o.x, xy.y + o.y}
}

func (xy *vec2i64) rotLeft() vec2i64 {
	return vec2i64{xy.y, -xy.x}
}

func (xy *vec2i64) rotRight() vec2i64 {
	return vec2i64{-xy.y, xy.x}
}

func (xy *vec2i64) inBounds(minx int64, miny int64, maxx int64, maxy int64) bool {
	return xy.x >= minx && xy.x <= maxx && xy.y >= miny && xy.y <= maxy
}

type RuneSlice []rune

func (p RuneSlice) Len() int           { return len(p) }
func (p RuneSlice) Less(i, j int) bool { return p[i] < p[j] }
func (p RuneSlice) Swap(i, j int)      { p[i], p[j] = p[j], p[i] }

func sortString(s string) string {
	runes := []rune(s)
	sort.Sort(RuneSlice(runes))
	return string(runes)
}

func containsAll(s string, list string) bool {
	for _, r := range list {
		if strings.ContainsRune(s, r) == false {
			// fmt.Println(s, "does not contain", string(r))
			return false
		}
	}
	// fmt.Println(s, "contains", list)
	return true
}

func ParseGrid(input string) [][]rune {
	ss := strings.Split(input, "\n")
	grid := [][]rune{}
	for _, s := range ss {
		grid = append(grid, []rune(s))
	}
	return grid
}

func isUpper(c rune) bool {
	return c >= 'A' && c <= 'Z'
}

func isLower(c rune) bool {
	return c >= 'a' && c <= 'z'
}

func asciiArray64(a []int64) string {
	s := ""
	for _, c := range a {
		s += string(rune(c))
	}
	return s
}
