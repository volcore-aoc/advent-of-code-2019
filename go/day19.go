package main

import "fmt"

func compute19a(input string) int64 {
	mem := parseInt64Array(input)
	count := int64(0)
	for y := int64(0); y < 50; y++ {
		for x := int64(0); x < 50; x++ {
			vm := createIntCodeVM(mem)
			go vm.run()
			vm.input <- x
			vm.input <- y
			value := <-vm.output
			<-vm.done
			// fmt.Print(value)
			count += value
		}
		// fmt.Println()
	}
	return count
}

func isInside(x, y int64, mem []int64) bool {
	vm := createIntCodeVM(mem)
	go vm.run()
	vm.input <- x
	vm.input <- y
	value := <-vm.output
	<-vm.done
	return value == 1
}

func compute19b(input string) int64 {
	mem := parseInt64Array(input)
	target := int64(100)
	y := int64(3)
	minx, maxx := int64(0), int64(0)
	for minx = 0; isInside(minx, y, mem) == false; minx++ {
	}
	mins, maxs := make(map[int64]int64), make(map[int64]int64)
	for ; ; y++ {
		for ; isInside(minx, y, mem) == false; minx++ {
		}
		for maxx = max64(maxx, minx); isInside(maxx, y, mem) == true; maxx++ {
		}
		mins[y], maxs[y] = minx, maxx
		// Check if it fits in here
		if maxs[y-target+1] >= mins[y]+target {
			return mins[y]*10000 + (y - target + 1)
		}
	}
}

func main() {
	fmt.Println("Result A", compute19a(load("../inputs/19.txt")))
	fmt.Println("Result B", compute19b(load("../inputs/19.txt")))
}
