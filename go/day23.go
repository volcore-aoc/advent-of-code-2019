package main

import (
	"fmt"
	"time"
)

type ctx23 struct {
	vms    []*intCodeVM
	result chan []int64
}

func (ctx *ctx23) router(idx int64) {
	for {
		select {
		case dst := <-ctx.vms[idx].output:
			{
				x := <-ctx.vms[idx].output
				y := <-ctx.vms[idx].output
				if dst == 255 {
					ctx.result <- []int64{x, y}
				} else {
					ctx.vms[dst].inputList <- []int64{x, y}
				}
				break
			}
		case <-ctx.vms[idx].done:
			{
				fmt.Println(idx, "done")
				return
			}
		}
	}
}

func startVMs23(input string) *ctx23 {
	vmCount := int64(50)
	ctx := &ctx23{
		vms:    make([]*intCodeVM, vmCount),
		result: make(chan []int64),
	}
	mem := parseInt64Array(input)
	for i := int64(0); i < vmCount; i++ {
		ctx.vms[i] = createIntCodeVMBuffered(mem, 10)
		go ctx.vms[i].run()
		ctx.vms[i].input <- i
	}
	for i := int64(0); i < vmCount; i++ {
		go ctx.router(i)
		ctx.vms[i].input <- -1
	}
	return ctx
}

func compute23a(input string) int64 {
	ctx := startVMs23(input)
	return (<-ctx.result)[0]
}

func compute23b(input string) int64 {
	ctx := startVMs23(input)
	mostRecent := []int64{0, 0}
	previous := []int64{0, 0}
	for {
		select {
		case list := <-ctx.result:
			{
				mostRecent = list[:]
				// fmt.Println("Received:", list)
				break
			}
		case <-time.After(100 * time.Millisecond):
			{
				if mostRecent[1] == previous[1] {
					// fmt.Println("Duplicate y value: ", previous[1])
					return previous[1]
				}
				previous = mostRecent[:]
				// fmt.Println("Timeout", mostRecent)
				ctx.vms[0].inputList <- mostRecent
				break
			}
		}
	}
	return 0
}

func main() {
	fmt.Println("Result A", compute23a(load("../inputs/23.txt")))
	fmt.Println("Result B", compute23b(load("../inputs/23.txt")))
}

// 13471 too high
// 12481 too high
// 12478 too high
