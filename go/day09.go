package main

import "fmt"

type vm09 struct {
	memMap        map[int64]int64
	ip            int64
	terminated    bool
	input         chan int64
	output        chan int64
	done          chan bool
	relBaseOffset int64
}

func createVM09(memory []int64) *vm09 {
	memMap := make(map[int64]int64)
	for idx, value := range memory {
		memMap[int64(idx)] = value
	}
	return &vm09{memMap,
		0,
		false,
		make(chan int64),
		make(chan int64),
		make(chan bool),
		0}
}

func (vm *vm09) mem(idx int64) int64 {
	return vm.memMap[idx]
}

func (vm *vm09) setmem(idx int64, value int64) {
	vm.memMap[idx] = value
}

func (vm *vm09) param(idx int64, mode int64) int64 {
	switch mode {
	case 0:
		{
			// reference
			return vm.mem(vm.mem(vm.ip + idx))
		}
	case 1:
		{
			// immediate
			return vm.mem(vm.ip + idx)
		}
	case 2:
		{
			// relative mode
			return vm.mem(vm.relBaseOffset + vm.mem(vm.ip+idx))
		}
	default:
		{
			fmt.Println("mode ", mode, " NYI")
			return 0
		}
	}
}

func (vm *vm09) paramOut(idx int64, mode int64) int64 {
	switch mode {
	case 0:
		{
			// reference
			return vm.mem(vm.ip + idx)
		}
	case 1:
		{
			// immediate
			return vm.mem(vm.ip + idx)
		}
	case 2:
		{
			// relative mode
			return vm.relBaseOffset + vm.mem(vm.ip+idx)
		}
	default:
		{
			fmt.Println("mode ", mode, " NYI")
			return 0
		}
	}
}

func (vm *vm09) step() {
	instruction := vm.mem(vm.ip)
	opCode := instruction % 100
	p1Mode := (instruction / 100) % 10
	p2Mode := (instruction / 1000) % 10
	p3Mode := (instruction / 10000) % 10
	// fmt.Println(instruction, opCode, p1Mode, p2Mode, p3Mode, vm.mem(vm.ip), vm.mem(vm.ip+1), vm.mem(vm.ip+2), vm.mem(vm.ip+3))
	switch opCode {
	case 1:
		{
			vm.setmem(vm.paramOut(3, p3Mode), vm.param(1, p1Mode)+vm.param(2, p2Mode))
			vm.ip += 4
			break
		}
	case 2:
		{
			vm.setmem(vm.paramOut(3, p3Mode), vm.param(1, p1Mode)*vm.param(2, p2Mode))
			vm.ip += 4
			break
		}
	case 3:
		{
			vm.setmem(vm.paramOut(1, p1Mode), <-vm.input)
			vm.ip += 2
			break
		}
	case 4:
		{
			vm.output <- vm.param(1, p1Mode)
			vm.ip += 2
			break
		}
	case 5:
		{
			if vm.param(1, p1Mode) != 0 {
				vm.ip = vm.param(2, p2Mode)
			} else {
				vm.ip += 3
			}
			break
		}
	case 6:
		{
			if vm.param(1, p1Mode) == 0 {
				vm.ip = vm.param(2, p2Mode)
			} else {
				vm.ip += 3
			}
			break
		}
	case 7:
		{
			res := int64(0)
			if vm.param(1, p1Mode) < vm.param(2, p2Mode) {
				res = 1
			}
			vm.setmem(vm.paramOut(3, p3Mode), res)
			vm.ip += 4
			break
		}
	case 8:
		{
			res := int64(0)
			if vm.param(1, p1Mode) == vm.param(2, p2Mode) {
				res = 1
			}
			vm.setmem(vm.paramOut(3, p3Mode), res)
			vm.ip += 4
			break
		}
	case 9:
		{
			vm.relBaseOffset += vm.param(1, p1Mode)
			vm.ip += 2
			break
		}
	default:
		{
			fmt.Println("Unknown instruction ", instruction)
			vm.terminated = true
			close(vm.done)
			break
		}
	case 99:
		{
			vm.terminated = true
			close(vm.done)
			break
		}
	}
}

func (vm *vm09) run() *vm09 {
	for {
		vm.step()
		if vm.terminated {
			return vm
		}
	}
}

func compute09a(code string, input []int64) int64 {
	mem := parseInt64Array(code)
	vm := createVM09(mem)
	go vm.run()
	for _, v := range input {
		vm.input <- v
	}
	var value int64
	for {
		select {
		case value = <-vm.output:
			fmt.Println("Output: ", value)
			break
		case <-vm.done:
			return value
		}
	}
}

func compute09aNoInput(code string) int64 {
	return compute09a(code, []int64{})
}

func main() {
	fmt.Println("*** Tests")
	test64(compute09aNoInput, "109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99", 99)
	test64(compute09aNoInput, "1102,34915192,34915192,7,4,7,99,0", 1219070632396864)
	test64(compute09aNoInput, "104,1125899906842624,99", 1125899906842624)
	fmt.Println("*** Part A")
	fmt.Println(compute09a(load("../inputs/09.txt"), []int64{1}))
	fmt.Println("*** Part B")
	fmt.Println(compute09a(load("../inputs/09.txt"), []int64{2}))
}
