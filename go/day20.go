package main

import "fmt"

type portal20 struct {
	exits []vec2i
}

type ctx20 struct {
	grid      [][]rune
	portals   map[string]*portal20
	portalLUT map[vec2i]string
	w         int
	h         int
}

func (ctx *ctx20) addPortal(name string, exit vec2i) {
	ctx.portalLUT[exit] = name
	if ctx.portals[name] == nil {
		ctx.portals[name] = new(portal20)
		ctx.portals[name].exits = make([]vec2i, 0)
	}
	ctx.portals[name].exits = append(ctx.portals[name].exits, exit)
}

func parse20(input string) *ctx20 {
	ctx := &ctx20{
		grid:      ParseGrid(input),
		portals:   make(map[string]*portal20),
		portalLUT: make(map[vec2i]string),
	}
	ctx.w = len(ctx.grid[0])
	ctx.h = len(ctx.grid)
	for y := 1; y < ctx.h-1; y++ {
		for x := 1; x < ctx.w-1; x++ {
			if isUpper(ctx.grid[y][x]) {
				if isUpper(ctx.grid[y][x-1]) && ctx.grid[y][x+1] == '.' {
					name := string(ctx.grid[y][x-1]) + string(ctx.grid[y][x])
					ctx.addPortal(name, vec2i{x + 1, y})
				} else if isUpper(ctx.grid[y][x+1]) && ctx.grid[y][x-1] == '.' {
					name := string(ctx.grid[y][x]) + string(ctx.grid[y][x+1])
					ctx.addPortal(name, vec2i{x - 1, y})
				} else if isUpper(ctx.grid[y-1][x]) && ctx.grid[y+1][x] == '.' {
					name := string(ctx.grid[y-1][x]) + string(ctx.grid[y][x])
					ctx.addPortal(name, vec2i{x, y + 1})
				} else if isUpper(ctx.grid[y+1][x]) && ctx.grid[y-1][x] == '.' {
					name := string(ctx.grid[y][x]) + string(ctx.grid[y+1][x])
					ctx.addPortal(name, vec2i{x, y - 1})
				}
			}
		}
	}
	return ctx
}

func (ctx *ctx20) score(pos vec2i, travelled int) int {
	return travelled + pos.dist1(ctx.portals["ZZ"].exits[0])
}

func (ctx *ctx20) search(extended bool) int {
	offsets := []vec3i{{-1, 0, 0}, {1, 0, 0}, {0, -1, 0}, {0, 1, 0}}
	start2 := ctx.portals["AA"].exits[0]
	start := vec3i{start2.x, start2.y, 0}
	distances := make(map[vec3i]int)
	distances[start] = 1
	queue := make(prioQueue, 0)
	queue.Put(&start, 1)
	best := 9999999999
	for queue.Len() > 0 {
		next := queue.Next()
		nextPos := next.value.(*vec3i)
		distance := distances[*nextPos]
		// fmt.Println(nextPos, distance)
		// If this is a portal cell, check the other side
		pname, ok := ctx.portalLUT[nextPos.xy()]
		if ok {
			outer := nextPos.x < 3 || nextPos.x >= ctx.w-3 || nextPos.y < 3 || nextPos.y >= ctx.h-3
			if pname == "ZZ" {
				if extended && nextPos.z != 0 {
					// Nothing happens on AA and ZZ if not on the 0 layer
				} else {
					if distance < best {
						best = distance
					}
				}
			}
			if extended && outer && nextPos.z == 0 {
				// Nothing happens to outer portals on the first layer
			} else {
				if len(ctx.portals[pname].exits) > 1 {
					nd := distance + 1
					np2 := ctx.portals[pname].exits[0]
					if np2 == nextPos.xy() {
						np2 = ctx.portals[pname].exits[1]
					}
					np := vec3i{np2.x, np2.y, nextPos.z}
					if extended {
						if outer {
							np.z--
						} else {
							np.z++
						}
					}
					if np.z >= 0 && (distances[np] == 0 || distances[np] > nd) {
						distances[np] = nd
						// fmt.Println("queueP", pname, nextPos, np, nd, outer)
						queue.Put(&np, nd+10000*np.z)
					}
				}
			}
		}
		for _, offset := range offsets {
			np := nextPos.add(offset)
			if ctx.grid[np.y][np.x] != '.' {
				continue
			}
			// fmt.Println("looking at ", np)
			nd := distance + 1
			if distances[np] != 0 && distances[np] <= nd {
				// Don't re-queue if already processed cheaper
				continue
			}
			if nd > best {
				// Don't queue if can't possibly be best
				continue
			}
			distances[np] = nd
			// fmt.Println("queue", np, nd)
			queue.Put(&np, nd+10000*np.z)
		}
	}
	// Dump the result
	if false {
		// Change layer max to print more layers
		for layer := 0; layer < 1; layer++ {
			fmt.Println("Layer", layer)
			for y := 0; y < ctx.h; y++ {
				line := ""
				for x := 0; x < ctx.w; x++ {
					if distances[vec3i{x, y, 0}] != 0 {
						line += "x"
					} else {
						line += string(ctx.grid[y][x])
					}
				}
				fmt.Println(line)
			}
		}
	}
	return best - 1
}

func compute20a(input string) int {
	ctx := parse20(input)
	return ctx.search(false)
}

func compute20b(input string) int {
	ctx := parse20(input)
	return ctx.search(true)
}

func main() {
	test(compute20a, "         A           \n         A           \n  #######.#########  \n  #######.........#  \n  #######.#######.#  \n  #######.#######.#  \n  #######.#######.#  \n  #####  B    ###.#  \nBC...##  C    ###.#  \n  ##.##       ###.#  \n  ##...DE  F  ###.#  \n  #####    G  ###.#  \n  #########.#####.#  \nDE..#######...###.#  \n  #.#########.###.#  \nFG..#########.....#  \n  ###########.#####  \n             Z       \n             Z       ", 23)
	test(compute20a, "                   A               \n                   A               \n  #################.#############  \n  #.#...#...................#.#.#  \n  #.#.#.###.###.###.#########.#.#  \n  #.#.#.......#...#.....#.#.#...#  \n  #.#########.###.#####.#.#.###.#  \n  #.............#.#.....#.......#  \n  ###.###########.###.#####.#.#.#  \n  #.....#        A   C    #.#.#.#  \n  #######        S   P    #####.#  \n  #.#...#                 #......VT\n  #.#.#.#                 #.#####  \n  #...#.#               YN....#.#  \n  #.###.#                 #####.#  \nDI....#.#                 #.....#  \n  #####.#                 #.###.#  \nZZ......#               QG....#..AS\n  ###.###                 #######  \nJO..#.#.#                 #.....#  \n  #.#.#.#                 ###.#.#  \n  #...#..DI             BU....#..LF\n  #####.#                 #.#####  \nYN......#               VT..#....QG\n  #.###.#                 #.###.#  \n  #.#...#                 #.....#  \n  ###.###    J L     J    #.#.###  \n  #.....#    O F     P    #.#...#  \n  #.###.#####.#.#####.#####.###.#  \n  #...#.#.#...#.....#.....#.#...#  \n  #.#####.###.###.#.#.#########.#  \n  #...#.#.....#...#.#.#.#.....#.#  \n  #.###.#####.###.###.#.#.#######  \n  #.#.........#...#.............#  \n  #########.###.###.#############  \n           B   J   C               \n           U   P   P               ", 58)
	fmt.Println("Result A", compute20a(loadNoTrim("../inputs/20.txt")))
	test(compute20b, "         A           \n         A           \n  #######.#########  \n  #######.........#  \n  #######.#######.#  \n  #######.#######.#  \n  #######.#######.#  \n  #####  B    ###.#  \nBC...##  C    ###.#  \n  ##.##       ###.#  \n  ##...DE  F  ###.#  \n  #####    G  ###.#  \n  #########.#####.#  \nDE..#######...###.#  \n  #.#########.###.#  \nFG..#########.....#  \n  ###########.#####  \n             Z       \n             Z       ", 26)
	test(compute20b, "             Z L X W       C                 \n             Z P Q B       K                 \n  ###########.#.#.#.#######.###############  \n  #...#.......#.#.......#.#.......#.#.#...#  \n  ###.#.#.#.#.#.#.#.###.#.#.#######.#.#.###  \n  #.#...#.#.#...#.#.#...#...#...#.#.......#  \n  #.###.#######.###.###.#.###.###.#.#######  \n  #...#.......#.#...#...#.............#...#  \n  #.#########.#######.#.#######.#######.###  \n  #...#.#    F       R I       Z    #.#.#.#  \n  #.###.#    D       E C       H    #.#.#.#  \n  #.#...#                           #...#.#  \n  #.###.#                           #.###.#  \n  #.#....OA                       WB..#.#..ZH\n  #.###.#                           #.#.#.#  \nCJ......#                           #.....#  \n  #######                           #######  \n  #.#....CK                         #......IC\n  #.###.#                           #.###.#  \n  #.....#                           #...#.#  \n  ###.###                           #.#.#.#  \nXF....#.#                         RF..#.#.#  \n  #####.#                           #######  \n  #......CJ                       NM..#...#  \n  ###.#.#                           #.###.#  \nRE....#.#                           #......RF\n  ###.###        X   X       L      #.#.#.#  \n  #.....#        F   Q       P      #.#.#.#  \n  ###.###########.###.#######.#########.###  \n  #.....#...#.....#.......#...#.....#.#...#  \n  #####.#.###.#######.#######.###.###.#.#.#  \n  #.......#.......#.#.#.#.#...#...#...#.#.#  \n  #####.###.#####.#.#.#.#.###.###.#.###.###  \n  #.......#.....#.#...#...............#...#  \n  #############.#.#.###.###################  \n               A O F   N                     \n               A A D   M                     ", 396)
	fmt.Println("Result B", compute20b(loadNoTrim("../inputs/20.txt")))
}
