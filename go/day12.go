package main

import (
	"fmt"
	"strconv"
	"strings"
)

type moon12 struct {
	pos []int64
	vel []int64
}

func (moon *moon12) String() string {
	return fmt.Sprintf("pos=<%d,%d,%d> vel=<%d,%d,%d>", moon.pos[0], moon.pos[1], moon.pos[2], moon.vel[0], moon.vel[1], moon.vel[2])
}

func (space *space12) String() string {
	s := ""
	for _, moon := range space.moons {
		s += moon.String() + "\n"
	}
	return s
}

type space12 struct {
	moons []*moon12
}

func parse(input string) *space12 {
	space := &space12{
		make([]*moon12, 0),
	}
	lines := strings.Split(input, "\n")
	for _, line := range lines {
		if len(line) == 0 {
			break
		}
		moon := &moon12{
			make([]int64, 3),
			make([]int64, 3),
		}
		line = line[1 : len(line)-1]
		comps := strings.Split(line, ",")
		for i := 0; i < 3; i++ {
			v, _ := strconv.Atoi(strings.Split(comps[i], "=")[1])
			moon.pos[i] = int64(v)
		}
		space.moons = append(space.moons, moon)
	}
	return space
}

func (space *space12) energy() int64 {
	energy := int64(0)
	for _, moon := range space.moons {
		pot := absInt64(moon.pos[0]) + absInt64(moon.pos[1]) + absInt64(moon.pos[2])
		kin := absInt64(moon.vel[0]) + absInt64(moon.vel[1]) + absInt64(moon.vel[2])
		energy += pot * kin
	}
	return energy
}

func (space *space12) stepVel() {
	numMoons := len(space.moons)
	for i := 0; i < numMoons; i++ {
		moonI := space.moons[i]
		for j := i + 1; j < numMoons; j++ {
			moonJ := space.moons[j]
			for axis := 0; axis < 3; axis++ {
				a := moonI.pos[axis]
				b := moonJ.pos[axis]
				if a == b {
					continue
				} else if a < b {
					moonI.vel[axis]++
					moonJ.vel[axis]--
				} else {
					moonI.vel[axis]--
					moonJ.vel[axis]++
				}
			}
		}
	}
}

func (space *space12) stepPos() {
	for _, moon := range space.moons {
		moon.pos[0] += moon.vel[0]
		moon.pos[1] += moon.vel[1]
		moon.pos[2] += moon.vel[2]
	}
}

func compute12a(input string, iterations int) int64 {
	space := parse(input)
	for i := 0; i < iterations; i++ {
		space.stepVel()
		space.stepPos()
		// fmt.Println("Step", i+1)
		// fmt.Println(space)
	}
	return space.energy()
}

func compute12aTest(iterations int) func(string) int64 {
	return func(input string) int64 {
		return compute12a(input, iterations)
	}
}

func compute12b(input string) int64 {
	inputSpace := parse(input)
	space := parse(input)
	axisCycle := make([]int64, 3)
	for i := int64(0); ; i++ {
		space.stepVel()
		space.stepPos()
		allFound := true
		for axis := 0; axis < 3; axis++ {
			if axisCycle[axis] != 0 {
				continue
			}
			cycle := true
			for idx := 0; idx < len(space.moons); idx++ {
				if space.moons[idx].pos[axis] != inputSpace.moons[idx].pos[axis] {
					cycle = false
				}
			}
			if cycle == true {
				axisCycle[axis] = i + 2
				fmt.Println("Axis", axis, " cycle ", i+2)
			} else {
				allFound = false
			}
		}
		if allFound {
			return lcm(axisCycle[0], axisCycle[1], axisCycle[2])
		}
	}
}

func main() {
	test64(compute12aTest(10), "<x=-1, y=0, z=2>\n<x=2, y=-10, z=-7>\n<x=4, y=-8, z=8>\n<x=3, y=5, z=-1>\n", 179)
	test64(compute12aTest(100), "<x=-8, y=-10, z=0>\n<x=5, y=5, z=10>\n<x=2, y=-7, z=3>\n<x=9, y=-8, z=-3>\n", 1940)
	fmt.Println("A result:", compute12a(load("../inputs/12.txt"), 1000))
	test64(compute12b, "<x=-1, y=0, z=2>\n<x=2, y=-10, z=-7>\n<x=4, y=-8, z=8>\n<x=3, y=5, z=-1>\n", 2772)
	test64(compute12b, "<x=-8, y=-10, z=0>\n<x=5, y=5, z=10>\n<x=2, y=-7, z=3>\n<x=9, y=-8, z=-3>\n", 4686774924)
	fmt.Println("B result:", compute12b(load("../inputs/12.txt")))
}
