package main

import (
	"bufio"
	"fmt"
	"os"
)

func compute25a(input string) {
	reader := bufio.NewReader(os.Stdin)
	mem := parseInt64Array(input)
	vm := createIntCodeVM(mem)
	go vm.run()
	eatUntilCommand(vm)
	autoplay(vm)
	if true {
		autosolve(vm)
	} else {
		for {
			cmd, _ := reader.ReadString('\n')
			vm.writeLine(cmd)
			if eatUntilCommand(vm) == false {
				fmt.Println("Terminated")
				return
			}
		}
	}
}

func eatUntilCommand(vm *intCodeVM) bool {
	for {
		line, ok := vm.readLine()
		fmt.Println(line)
		if !ok {
			fmt.Println("VM terminated")
			return false
		}
		if line == "Command?" {
			return true
		}
	}
}

func autosolve(vm *intCodeVM) {
	items := []string{
		"hypercube",
		"space law space brochure",
		"festive hat",
		"boulder",
		"shell",
		"astronaut ice cream",
		"mug",
		"whirled peas",
	}
	for i := 0; i < 256; i++ {
		// Drop the unneeded items
		for j := uint(0); j < 8; j++ {
			if i&(1<<j) == 0 {
				vm.writeLine("drop " + items[j])
				eatUntilCommand(vm)
			}
		}
		vm.writeLine("inv")
		if eatUntilCommand(vm) == false {
			return
		}
		vm.writeLine("south")
		if eatUntilCommand(vm) == false {
			return
		}
		// Take back all items
		for j := uint(0); j < 8; j++ {
			if i&(1<<j) == 0 {
				vm.writeLine("take " + items[j])
				eatUntilCommand(vm)
			}
		}
	}
}

func autoplay(vm *intCodeVM) {
	commandIndex := 0
	commands := []string{
		"west",
		"take hypercube",
		"west",
		"take space law space brochure",
		"west",
		"north",
		"take shell",
		"west",
		"take mug",
		"south",
		"take festive hat",
		"north",
		"east",
		"south",
		"east",
		"east",
		"east",
		"south",
		"east",
		"take boulder",
		"west",
		"north",
		"east",
		"north",
		"west",
		"north",
		"take whirled peas",
		"west",
		"west",
		"take astronaut ice cream",
		"south",
	}
	for {
		fmt.Println(commands[commandIndex])
		vm.writeLine(commands[commandIndex])
		commandIndex++
		if eatUntilCommand(vm) == false {
			return
		}
		if commandIndex >= len(commands) {
			fmt.Println("**** Finished autoplay")
			return
		}
	}
}

func main() {
	input := load("../inputs/25.txt")
	compute25a(input)
}
