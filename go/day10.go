package main

import (
	"fmt"
	"math"
	"sort"
	"strings"
)

type ctx10 struct {
	grid   string
	width  int
	height int
}

func (ctx *ctx10) draw(visited []bool, counted []bool, cx int, cy int, count int) {
	fmt.Println("*** ", cx, cy, count)
	for y := 0; y < ctx.height; y++ {
		line := ""
		for x := 0; x < ctx.width; x++ {
			idx := x + y*ctx.width
			c := ctx.grid[idx]
			if visited[idx] {
				if ctx.grid[idx] == '#' {
					if counted[idx] {
						c = '+'
					} else {
						c = '-'
					}
				} else {
					c = ' '
				}
			}
			if cx == x && cy == y {
				c = 'X'
			}
			line += string(c)
		}
		fmt.Println(line)
	}
}

func (ctx *ctx10) occlude(visited []bool, x int, y int, ox int, oy int) {
	fx, fy := normal(ox, oy)
	// fmt.Println(ox, oy, "|", fx, fy)
	// Find all remaining asteroids with the same normal
	for yp := 0; yp < ctx.height; yp++ {
		for xp := 0; xp < ctx.width; xp++ {
			if (x == xp) && (y == yp) {
				continue
			}
			idx := xp + ctx.width*yp
			if ctx.grid[idx] != '#' {
				continue
			}
			if visited[idx] {
				continue
			}
			fxp, fyp := normal(xp-x, yp-y)
			if (math.Abs(fxp-fx) >= 0.00001) || (math.Abs(fyp-fy) >= 0.00001) {
				continue
			}
			// fmt.Println(xp, yp, ">", fxp, fyp, "==", fx, fy)
			visited[idx] = true
		}
	}
}

func (ctx *ctx10) visForCoord(x int, y int) int {
	count := 0
	// Allocate the entire grid as flags for what has been visited already
	visited := make([]bool, ctx.width*ctx.height)
	counted := make([]bool, ctx.width*ctx.height)
	// For every cell starting from the center (+- width/height)
	offsets := [][]int{[]int{1, 1}, []int{-1, 1}, []int{1, -1}, []int{-1, -1}}
	for dy := 0; dy < ctx.height; dy++ {
		for dx := 0; dx < ctx.width; dx++ {
			if (dy == 0) && (dx == 0) {
				continue
			}
			// for each mutation of +-xy
			for _, off := range offsets {
				// Increments
				ix := off[0] * dx
				iy := off[1] * dy
				// Target position
				tx := ix + x
				ty := iy + y
				if tx < 0 || tx >= ctx.width || ty < 0 || ty >= ctx.height {
					continue
				}
				idx := tx + ty*ctx.width
				if visited[idx] {
					continue
				}
				visited[idx] = true
				if ctx.grid[idx] == '.' {
					// Nothing hit, continue
					continue
				}
				counted[idx] = true
				count++
				ctx.occlude(visited, x, y, ix, iy)
			}
		}
	}
	// ctx.draw(visited, counted, x, y, count)
	return count
}

func (ctx *ctx10) findBest() int {
	max := 0
	for y := 0; y < ctx.height; y++ {
		for x := 0; x < ctx.width; x++ {
			if ctx.grid[x+y*ctx.width] != '#' {
				continue
			}
			val := ctx.visForCoord(x, y)
			if val > max {
				max = val
			}
		}
	}
	return max
}

func compute10a(input string) int {
	rows := strings.Split(strings.Trim(input, "\n \t"), "\n")
	for _, row := range rows {
		if len(row) != len(rows) {
			fmt.Println("Not square!")
			return -1
		}
	}
	ctx10 := &ctx10{strings.Join(rows, ""), len(rows), len(rows)}
	return ctx10.findBest()
}

type Asteroid10 struct {
	Angle    float64
	Distance int
	X, Y     int
}

func (a Asteroid10) String() string {
	return fmt.Sprintf("{%f %d %d,%d}", a.Angle, a.Distance, a.X, a.Y)
}

func (ctx *ctx10) getCoord(x int, y int, index int) int {
	// Generate a list of angles
	angles := make([]*Asteroid10, 0)
	for ty := 0; ty < ctx.height; ty++ {
		for tx := 0; tx < ctx.width; tx++ {
			idx := tx + ty*ctx.width
			if ctx.grid[idx] != '#' {
				continue
			}
			dx := tx - x
			dy := ty - y
			distance := dx*dx + dy*dy
			angle := computeAngle(dx, dy)
			angle = math.Mod(angle+math.Pi*10.0/4.0, 2.0*math.Pi)
			angle = math.Round(angle*1000.0) / 1000.0
			angles = append(angles, &Asteroid10{angle, distance, tx, ty})
		}
	}
	// Sort
	sort.Slice(angles, func(i, j int) bool {
		if angles[i].Angle == angles[j].Angle {
			return angles[i].Distance < angles[j].Distance
		}
		return angles[i].Angle < angles[j].Angle
	})
	// start eliminating
	current := -1.0
	result := 0
	for i := 0; i < index; i++ {
		newAngles, rem := removeNext(angles, current)
		if len(angles) == len(newAngles) {
			panic("didn't remove an angle from the list!")
		}
		angles = newAngles
		current = rem.Angle
		result = rem.X*100 + rem.Y
		// fmt.Println(i+1, result)
	}
	return result
}

func removeNext(angles []*Asteroid10, cur float64) ([]*Asteroid10, *Asteroid10) {
	for idx, angle := range angles {
		if angle.Angle <= cur {
			continue
		}
		if idx == 0 {
			return angles[1:], angle
		}
		if idx == len(angles)-1 {
			return angles[:len(angles)-1], angle
		}
		return append(angles[:idx-1], angles[idx+1:]...), angle
	}
	return angles, nil
}

func compute10b(input string, x int, y int) int {
	rows := strings.Split(strings.Trim(input, "\n \t"), "\n")
	ctx10 := &ctx10{strings.Join(rows, ""), len(rows), len(rows)}
	return ctx10.getCoord(x, y, 200)
}

func compute10bTest(input string) int {
	return compute10b(input, 11, 13)
}

func main() {
	test(compute10a, ".#..#\n.....\n#####\n....#\n...##\n", 8)
	test(compute10a, "......#.#.\n#..#.#....\n..#######.\n.#.#.###..\n.#..#.....\n..#....#.#\n#..#....#.\n.##.#..###\n##...#..#.\n.#....####\n", 33)
	test(compute10a, "#.#...#.#.\n.###....#.\n.#....#...\n##.#.#.#.#\n....#.#.#.\n.##..###.#\n..#...##..\n..##....##\n......#...\n.####.###.\n", 35)
	test(compute10a, ".#..#..###\n####.###.#\n....###.#.\n..###.##.#\n##.##.#.#.\n....###..#\n..#.#..#.#\n#..#.#.###\n.##...##.#\n.....#.#..\n", 41)
	test(compute10a, ".#..##.###...#######\n##.############..##.\n.#.######.########.#\n.###.#######.####.#.\n#####.##.#.##.###.##\n..#####..#.#########\n####################\n#.####....###.#.#.##\n##.#################\n#####.##.###..####..\n..######..##.#######\n####.##.####...##..#\n.#####..#.######.###\n##...#.##########...\n#.##########.#######\n.####.#.###.###.#.##\n....##.##.###..#####\n.#.#.###########.###\n#.#.#.#####.####.###\n###.##.####.##.#..##\n", 210)
	fmt.Println("A result", compute10a(load("../inputs/10.txt")))
	test(compute10bTest, ".#..##.###...#######\n##.############..##.\n.#.######.########.#\n.###.#######.####.#.\n#####.##.#.##.###.##\n..#####..#.#########\n####################\n#.####....###.#.#.##\n##.#################\n#####.##.###..####..\n..######..##.#######\n####.##.####...##..#\n.#####..#.######.###\n##...#.##########...\n#.##########.#######\n.####.#.###.###.#.##\n....##.##.###..#####\n.#.#.###########.###\n#.#.#.#####.####.###\n###.##.####.##.#..##\n", 802)
	fmt.Println("B result", compute10b(load("../inputs/10.txt"), 20, 21))
}
