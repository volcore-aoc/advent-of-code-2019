package main

import "fmt"

func compute21a(input string) int64 {
	mem := parseInt64Array(input)
	vm := createIntCodeVM(mem)
	go vm.run()
	for {
		line, ok := vm.readLine64()
		if ok == false {
			fmt.Println("Terminated")
			return line[0]
		}
		sline := asciiArray64(line)
		fmt.Println(sline)
		if sline == "Input instructions:" {
			vm.writeLine("NOT A J")
			vm.writeLine("NOT B T")
			vm.writeLine("OR T J")
			vm.writeLine("NOT C T")
			vm.writeLine("OR T J")
			vm.writeLine("AND D J")
			vm.writeLine("WALK")
			vm.writeLine("")
		}
	}
	return 0
}

// jump = d & (!a|!b|!c)

// Truth table (left side is inverted because derp)
// 0000 0
// 0001 0
// 0010 1
// 0011 0
// 0100 1
// 0101 0
// 0110 1
// 0111 0
// 1000 1
// 1001 0
// 1010 1
// 1011 0
// 1100 1
// 1101 0
// 1110 1
// 1111 0

func compute21b(input string) int64 {
	mem := parseInt64Array(input)
	vm := createIntCodeVM(mem)
	go vm.run()
	for {
		line, ok := vm.readLine64()
		if ok == false {
			fmt.Println("Terminated")
			if len(line) > 0 {
				return line[0]
			}
			return 0
		}
		sline := asciiArray64(line)
		fmt.Println(sline)
		if sline == "Input instructions:" {
			// jump = (d & (h | e)) & (!a|!b|!c)
			vm.writeLine("NOT A J")
			vm.writeLine("NOT B T")
			vm.writeLine("OR T J")
			vm.writeLine("NOT C T")
			vm.writeLine("OR T J")
			vm.writeLine("AND D J")
			// The not is here to set T to 0, to make sure it doesn't carry over from
			// before. Since this part of the clause is only relevant if D = 1,
			// not d will be 0
			vm.writeLine("NOT D T")
			vm.writeLine("OR E T")
			vm.writeLine("OR H T")
			vm.writeLine("AND T J")
			vm.writeLine("RUN")
			vm.writeLine("")
		}
	}
	return 0
}

func main() {
	fmt.Println("Result A", compute21a(load("../inputs/21.txt")))
	fmt.Println("Result B", compute21b(load("../inputs/21.txt")))
}
