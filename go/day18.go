package main

import (
	"fmt"
	"strings"
)

type ctx18 struct {
	grid  [][]rune
	start vec2i
	all   int
	keys  map[vec2i]int
	doors map[vec2i]int
}

func parse(input string) *ctx18 {
	ss := strings.Split(strings.TrimSpace(input), "\n")
	grid := [][]rune{}
	for _, s := range ss {
		grid = append(grid, []rune(s))
	}
	ctx := &ctx18{
		grid:  grid,
		keys:  make(map[vec2i]int),
		doors: make(map[vec2i]int),
	}
	// find the starting position and all keys
	for y, l := range ctx.grid {
		for x, r := range l {
			if r == '@' {
				ctx.start = vec2i{x, y}
			} else if r >= 'a' && r <= 'z' {
				ctx.keys[vec2i{x, y}] = 1 << uint(r-'a')
				ctx.all |= 1 << uint(r-'a')
			} else if r >= 'A' && r <= 'Z' {
				ctx.doors[vec2i{x, y}] = 1 << uint(r-'A')
			}
		}
	}
	return ctx
}

func (ctx *ctx18) score(p vec3i, d int) int {
	return d
}

func (ctx *ctx18) findShortestSequence() int {
	offsets := []vec3i{{-1, 0, 0}, {1, 0, 0}, {0, -1, 0}, {0, 1, 0}}
	distances := make(map[vec3i]int)
	queue := make(prioQueue, 0)
	start := vec3i{ctx.start.x, ctx.start.y, 0}
	queue.Put(&start, ctx.score(start, 0))
	for queue.Len() > 0 {
		next := queue.Next()
		nextPos := next.value.(*vec3i)
		if nextPos.z == ctx.all {
			return next.score
		}
		knownDistance := distances[*nextPos]
		if knownDistance != 0 && knownDistance <= next.score {
			// We've been here before and it was cheaper, so skip
			continue
		}
		distances[*nextPos] = next.score
		// Look at neighbors
		for _, offset := range offsets {
			np := nextPos.add(offset)
			// is this cell not passable?
			if ctx.grid[np.y][np.x] == '#' {
				continue
			}
			// is there a door that we can not pass?
			door, ok := ctx.doors[np.xy()]
			if ok && (door&np.z) == 0 {
				continue
			}
			// is there a key here, pick it up (if we have it already, or doesn't do
			// anything)
			key, ok := ctx.keys[np.xy()]
			if ok {
				np.z |= key
			}
			// This is walkable, so continue
			queue.Put(&np, next.score+1)
		}
	}
	return 0
}

func compute18a(input string) int {
	ctx := parse(input)
	return ctx.findShortestSequence()
}

// Can't use array here, since array doesn't work with map. So need to hack
// around that.
type state18b struct {
	pos0 vec2i
	pos1 vec2i
	pos2 vec2i
	pos3 vec2i
	keys int
}

func (state *state18b) pos(i int) *vec2i {
	switch i {
	case 0:
		return &state.pos0
	case 1:
		return &state.pos1
	case 2:
		return &state.pos2
	case 3:
		return &state.pos3
	}
	return nil
}

func (state *state18b) setpos(i int, v vec2i) {
	switch i {
	case 0:
		state.pos0 = v
		break
	case 1:
		state.pos1 = v
		break
	case 2:
		state.pos2 = v
		break
	case 3:
		state.pos3 = v
		break
	}
}

type reachableKey18 struct {
	key      int
	distance int
	pos      vec2i
}

func (ctx *ctx18) reachableKeys(pos vec2i, keys int) []reachableKey18 {
	offsets := []vec2i{{-1, 0}, {1, 0}, {0, -1}, {0, 1}}
	reach := []reachableKey18{}
	seen := make(map[vec2i]bool)
	queue := make(prioQueue, 0)
	queue.Put(&pos, 0)
	for queue.Len() > 0 {
		next := queue.Next()
		nextPos := next.value.(*vec2i)
		if seen[*nextPos] {
			continue
		}
		seen[*nextPos] = true
		// Look at neighbors
		for _, offset := range offsets {
			np := nextPos.add(offset)
			// is this cell not passable?
			if ctx.grid[np.y][np.x] == '#' {
				continue
			}
			// is there a door that we can not pass?
			door, ok := ctx.doors[np]
			if ok && (door&keys) == 0 {
				continue
			}
			// is there a key here, pick it up if it's a new key
			key, ok := ctx.keys[np]
			if ok && (key&keys) == 0 {
				// Also don't recurse from here, as we're only going to pick up one key
				// anyway
				reach = append(reach, reachableKey18{
					key, next.score + 1, np,
				})
				continue
			}
			// This is walkable, so continue
			queue.Put(&np, next.score+1)
		}
	}
	return reach
}

func compute18b(input string) int {
	ctx := parse(input)
	// patch
	ctx.grid[ctx.start.y][ctx.start.x] = '#'
	ctx.grid[ctx.start.y-1][ctx.start.x] = '#'
	ctx.grid[ctx.start.y+1][ctx.start.x] = '#'
	ctx.grid[ctx.start.y][ctx.start.x-1] = '#'
	ctx.grid[ctx.start.y][ctx.start.x+1] = '#'
	// Start the search
	distances := make(map[state18b]int)
	queue := make(prioQueue, 0)
	start := state18b{
		pos0: vec2i{ctx.start.x + 1, ctx.start.y - 1},
		pos1: vec2i{ctx.start.x - 1, ctx.start.y - 1},
		pos2: vec2i{ctx.start.x - 1, ctx.start.y + 1},
		pos3: vec2i{ctx.start.x + 1, ctx.start.y + 1},
		keys: 0,
	}
	queue.Put(&start, 0)
	for queue.Len() > 0 {
		next := queue.Next()
		nextState := next.value.(*state18b)
		if nextState.keys == ctx.all {
			return next.score
		}
		knownDistance := distances[*nextState]
		if knownDistance != 0 && knownDistance <= next.score {
			// We've been here before and it was cheaper, so skip
			continue
		}
		distances[*nextState] = next.score
		// Look at neighbors for all positions
		for corner := 0; corner < 4; corner++ {
			for _, reachable := range ctx.reachableKeys(*nextState.pos(corner), nextState.keys) {
				newState := *nextState
				newState.setpos(corner, reachable.pos)
				newState.keys |= reachable.key
				queue.Put(&newState, next.score+reachable.distance)
			}
		}
	}
	return 0
}

func main() {
	test(compute18a, "#########\n#b.A.@.a#\n#########", 8)
	test(compute18a, "########################\n#f.D.E.e.C.b.A.@.a.B.c.#\n######################.#\n#d.....................#\n########################", 86)
	test(compute18a, "########################\n#...............b.C.D.f#\n#.######################\n#.....@.a.B.c.d.A.e.F.g#\n########################", 132)
	test(compute18a, "#################\n#i.G..c...e..H.p#\n########.########\n#j.A..b...f..D.o#\n########@########\n#k.E..a...g..B.n#\n########.########\n#l.F..d...h..C.m#\n#################", 136)
	test(compute18a, "########################\n#@..............ac.GI.b#\n###d#e#f################\n###A#B#C################\n###g#h#i################\n########################", 81)
	fmt.Println("Result A", compute18a(load("../inputs/18.txt")))
	test(compute18b, "#######\n#a.#Cd#\n##...##\n##.@.##\n##...##\n#cB#Ab#\n#######", 8)
	test(compute18b, "###############\n#d.ABC.#.....a#\n######...######\n######.@.######\n######...######\n#b.....#.....c#\n###############", 24)
	test(compute18b, "#############\n#DcBa.#.GhKl#\n#.###...#I###\n#e#d#.@.#j#k#\n###C#...###J#\n#fEbA.#.FgHi#\n#############", 32)
	test(compute18b, "#############\n#g#f.D#..h#l#\n#F###e#E###.#\n#dCba...BcIJ#\n#####.@.#####\n#nK.L...G...#\n#M###N#H###.#\n#o#m..#i#jk.#\n#############", 72)
	fmt.Println("Result B", compute18b(load("../inputs/18.txt")))
}
