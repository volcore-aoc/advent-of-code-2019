package main

import (
	"fmt"
	"strings"
)

type ingredient14 struct {
	kind  string
	count int64
}

type rule14 struct {
	left  []ingredient14
	right ingredient14
}

type ctx14 struct {
	rules map[string]rule14
}

func (ctx *ctx14) compute(fuelCount int64) int64 {
	// fmt.Println(ctx.rules)
	oreCost := int64(0)
	queue := make(map[string]int64)
	leftOversPerFuel := make(map[string]int64)
	queue["FUEL"] = fuelCount
	for {
		// If queue is empty we're done
		if len(queue) == 0 {
			break
		}
		// Go through queue
		newQueue := make(map[string]int64)
		for k, v := range queue {
			needed := v
			// fmt.Println("Next element ", k, needed)
			rule := ctx.rules[k]
			// Check if we have leftOvers and subtrace
			left := leftOversPerFuel[k]
			if left > 0 {
				// fmt.Println("Using left overs", left, needed)
				if left <= needed {
					needed -= left
					leftOversPerFuel[k] = 0
				} else {
					leftOversPerFuel[k] = left - needed
					needed = 0
				}
			}
			if needed == 0 {
				continue
			}
			// Compute how many we need to produce
			countNeeded := (needed + rule.right.count - 1) / rule.right.count
			created := rule.right.count * countNeeded
			// fmt.Println("Created ", rule.right.kind, created)
			for _, sub := range rule.left {
				fullCount := sub.count * countNeeded
				if sub.kind == "ORE" {
					// Final result, add up
					oreCost += fullCount
					// fmt.Println("Created ORE", fullCount, ore)
				} else {
					// Add to queue
					// fmt.Println("queueing ", sub.kind, fullCount)
					newQueue[sub.kind] = newQueue[sub.kind] + fullCount
				}
			}
			if needed != created {
				delta := created - needed
				// fmt.Println("Created leftovers ", k, delta)
				leftOversPerFuel[k] = leftOversPerFuel[k] + delta
			}
		}
		queue = newQueue
		// fmt.Println(queue)
	}
	return oreCost
}

func (ctx *ctx14) computeA() int64 {
	return ctx.compute(1)
}

func (ctx *ctx14) computeB() int64 {
	trillion := int64(1000000000000)
	min := int64(1)
	max := int64(trillion)
	for {
		if min == max-1 {
			return min
		}
		newValue := (min + max) / 2
		res := ctx.compute(newValue)
		if res > trillion {
			max = newValue
		} else {
			min = newValue
		}
	}
}

func parseIngredient(s string) ingredient14 {
	list := strings.Split(strings.Trim(s, " "), " ")
	return ingredient14{
		list[1],
		ParseS64(list[0]),
	}
}

func parse14(input string) *ctx14 {
	rules := make(map[string]rule14)
	for _, line := range strings.Split(input, "\n") {
		sides := strings.Split(line, "=>")
		left := make([]ingredient14, 0)
		for _, ing := range strings.Split(sides[0], ",") {
			left = append(left, parseIngredient(ing))
		}
		right := parseIngredient(sides[1])
		rules[right.kind] = rule14{
			left,
			right,
		}
	}
	return &ctx14{rules}
}

func compute14a(input string) int64 {
	ctx := parse14(input)
	return ctx.computeA()
}

func compute14b(input string) int64 {
	ctx := parse14(input)
	// ore := 1000000000000
	return ctx.computeB()
}

func main() {
	test64(compute14a, "10 ORE => 10 A\n1 ORE => 1 B\n7 A, 1 B => 1 C\n7 A, 1 C => 1 D\n7 A, 1 D => 1 E\n7 A, 1 E => 1 FUEL", 31)
	test64(compute14a, "9 ORE => 2 A\n8 ORE => 3 B\n7 ORE => 5 C\n3 A, 4 B => 1 AB\n5 B, 7 C => 1 BC\n4 C, 1 A => 1 CA\n2 AB, 3 BC, 4 CA => 1 FUEL", 165)
	test64(compute14a, "157 ORE => 5 NZVS\n165 ORE => 6 DCFZ\n44 XJWVT, 5 KHKGT, 1 QDVJ, 29 NZVS, 9 GPVTF, 48 HKGWZ => 1 FUEL\n12 HKGWZ, 1 GPVTF, 8 PSHF => 9 QDVJ\n179 ORE => 7 PSHF\n177 ORE => 5 HKGWZ\n7 DCFZ, 7 PSHF => 2 XJWVT\n165 ORE => 2 GPVTF\n3 DCFZ, 7 NZVS, 5 HKGWZ, 10 PSHF => 8 KHKGT", 13312)
	test64(compute14a, "2 VPVL, 7 FWMGM, 2 CXFTF, 11 MNCFX => 1 STKFG\n17 NVRVD, 3 JNWZP => 8 VPVL\n53 STKFG, 6 MNCFX, 46 VJHF, 81 HVMC, 68 CXFTF, 25 GNMV => 1 FUEL\n22 VJHF, 37 MNCFX => 5 FWMGM\n139 ORE => 4 NVRVD\n144 ORE => 7 JNWZP\n5 MNCFX, 7 RFSQX, 2 FWMGM, 2 VPVL, 19 CXFTF => 3 HVMC\n5 VJHF, 7 MNCFX, 9 VPVL, 37 CXFTF => 6 GNMV\n145 ORE => 6 MNCFX\n1 NVRVD => 8 CXFTF\n1 VJHF, 6 MNCFX => 4 RFSQX\n176 ORE => 6 VJHF", 180697)
	test64(compute14a, "171 ORE => 8 CNZTR\n7 ZLQW, 3 BMBT, 9 XCVML, 26 XMNCP, 1 WPTQ, 2 MZWV, 1 RJRHP => 4 PLWSL\n114 ORE => 4 BHXH\n14 VRPVC => 6 BMBT\n6 BHXH, 18 KTJDG, 12 WPTQ, 7 PLWSL, 31 FHTLT, 37 ZDVW => 1 FUEL\n6 WPTQ, 2 BMBT, 8 ZLQW, 18 KTJDG, 1 XMNCP, 6 MZWV, 1 RJRHP => 6 FHTLT\n15 XDBXC, 2 LTCX, 1 VRPVC => 6 ZLQW\n13 WPTQ, 10 LTCX, 3 RJRHP, 14 XMNCP, 2 MZWV, 1 ZLQW => 1 ZDVW\n5 BMBT => 4 WPTQ\n189 ORE => 9 KTJDG\n1 MZWV, 17 XDBXC, 3 XCVML => 2 XMNCP\n12 VRPVC, 27 CNZTR => 2 XDBXC\n15 KTJDG, 12 BHXH => 5 XCVML\n3 BHXH, 2 VRPVC => 7 MZWV\n121 ORE => 7 VRPVC\n7 XCVML => 6 RJRHP\n5 BHXH, 4 VRPVC => 5 LTCX", 2210736)
	fmt.Println("Result A", compute14a(load("../inputs/14.txt")))
	test64(compute14b, "157 ORE => 5 NZVS\n165 ORE => 6 DCFZ\n44 XJWVT, 5 KHKGT, 1 QDVJ, 29 NZVS, 9 GPVTF, 48 HKGWZ => 1 FUEL\n12 HKGWZ, 1 GPVTF, 8 PSHF => 9 QDVJ\n179 ORE => 7 PSHF\n177 ORE => 5 HKGWZ\n7 DCFZ, 7 PSHF => 2 XJWVT\n165 ORE => 2 GPVTF\n3 DCFZ, 7 NZVS, 5 HKGWZ, 10 PSHF => 8 KHKGT", 82892753)
	test64(compute14b, "2 VPVL, 7 FWMGM, 2 CXFTF, 11 MNCFX => 1 STKFG\n17 NVRVD, 3 JNWZP => 8 VPVL\n53 STKFG, 6 MNCFX, 46 VJHF, 81 HVMC, 68 CXFTF, 25 GNMV => 1 FUEL\n22 VJHF, 37 MNCFX => 5 FWMGM\n139 ORE => 4 NVRVD\n144 ORE => 7 JNWZP\n5 MNCFX, 7 RFSQX, 2 FWMGM, 2 VPVL, 19 CXFTF => 3 HVMC\n5 VJHF, 7 MNCFX, 9 VPVL, 37 CXFTF => 6 GNMV\n145 ORE => 6 MNCFX\n1 NVRVD => 8 CXFTF\n1 VJHF, 6 MNCFX => 4 RFSQX\n176 ORE => 6 VJHF", 5586022)
	test64(compute14b, "171 ORE => 8 CNZTR\n7 ZLQW, 3 BMBT, 9 XCVML, 26 XMNCP, 1 WPTQ, 2 MZWV, 1 RJRHP => 4 PLWSL\n114 ORE => 4 BHXH\n14 VRPVC => 6 BMBT\n6 BHXH, 18 KTJDG, 12 WPTQ, 7 PLWSL, 31 FHTLT, 37 ZDVW => 1 FUEL\n6 WPTQ, 2 BMBT, 8 ZLQW, 18 KTJDG, 1 XMNCP, 6 MZWV, 1 RJRHP => 6 FHTLT\n15 XDBXC, 2 LTCX, 1 VRPVC => 6 ZLQW\n13 WPTQ, 10 LTCX, 3 RJRHP, 14 XMNCP, 2 MZWV, 1 ZLQW => 1 ZDVW\n5 BMBT => 4 WPTQ\n189 ORE => 9 KTJDG\n1 MZWV, 17 XDBXC, 3 XCVML => 2 XMNCP\n12 VRPVC, 27 CNZTR => 2 XDBXC\n15 KTJDG, 12 BHXH => 5 XCVML\n3 BHXH, 2 VRPVC => 7 MZWV\n121 ORE => 7 VRPVC\n7 XCVML => 6 RJRHP\n5 BHXH, 4 VRPVC => 5 LTCX", 460664)
	fmt.Println("Result B", compute14b(load("../inputs/14.txt")))
}
