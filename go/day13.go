package main

import "fmt"

type vm13 struct {
	memMap        map[int64]int64
	ip            int64
	terminated    bool
	input         chan int64
	output        chan int64
	done          chan bool
	relBaseOffset int64
}

func createVM13(memory []int64) *vm13 {
	memMap := make(map[int64]int64)
	for idx, value := range memory {
		memMap[int64(idx)] = value
	}
	return &vm13{memMap,
		0,
		false,
		make(chan int64, 2),
		make(chan int64, 2),
		make(chan bool),
		0}
}

func (vm *vm13) mem(idx int64) int64 {
	return vm.memMap[idx]
}

func (vm *vm13) setmem(idx int64, value int64) {
	vm.memMap[idx] = value
}

func (vm *vm13) param(idx int64, mode int64) int64 {
	switch mode {
	case 0:
		{
			// reference
			return vm.mem(vm.mem(vm.ip + idx))
		}
	case 1:
		{
			// immediate
			return vm.mem(vm.ip + idx)
		}
	case 2:
		{
			// relative mode
			return vm.mem(vm.relBaseOffset + vm.mem(vm.ip+idx))
		}
	default:
		{
			fmt.Println("mode ", mode, " NYI")
			return 0
		}
	}
}

func (vm *vm13) paramOut(idx int64, mode int64) int64 {
	switch mode {
	case 0:
		{
			// reference
			return vm.mem(vm.ip + idx)
		}
	case 1:
		{
			// immediate
			return vm.mem(vm.ip + idx)
		}
	case 2:
		{
			// relative mode
			return vm.relBaseOffset + vm.mem(vm.ip+idx)
		}
	default:
		{
			fmt.Println("mode ", mode, " NYI")
			return 0
		}
	}
}

func (vm *vm13) step() {
	instruction := vm.mem(vm.ip)
	opCode := instruction % 100
	p1Mode := (instruction / 100) % 10
	p2Mode := (instruction / 1000) % 10
	p3Mode := (instruction / 10000) % 10
	// fmt.Println(instruction, opCode, p1Mode, p2Mode, p3Mode, vm.mem(vm.ip), vm.mem(vm.ip+1), vm.mem(vm.ip+2), vm.mem(vm.ip+3))
	switch opCode {
	case 1:
		{
			vm.setmem(vm.paramOut(3, p3Mode), vm.param(1, p1Mode)+vm.param(2, p2Mode))
			vm.ip += 4
			break
		}
	case 2:
		{
			vm.setmem(vm.paramOut(3, p3Mode), vm.param(1, p1Mode)*vm.param(2, p2Mode))
			vm.ip += 4
			break
		}
	case 3:
		{
			vm.setmem(vm.paramOut(1, p1Mode), <-vm.input)
			vm.ip += 2
			break
		}
	case 4:
		{
			vm.output <- vm.param(1, p1Mode)
			vm.ip += 2
			break
		}
	case 5:
		{
			if vm.param(1, p1Mode) != 0 {
				vm.ip = vm.param(2, p2Mode)
			} else {
				vm.ip += 3
			}
			break
		}
	case 6:
		{
			if vm.param(1, p1Mode) == 0 {
				vm.ip = vm.param(2, p2Mode)
			} else {
				vm.ip += 3
			}
			break
		}
	case 7:
		{
			res := int64(0)
			if vm.param(1, p1Mode) < vm.param(2, p2Mode) {
				res = 1
			}
			vm.setmem(vm.paramOut(3, p3Mode), res)
			vm.ip += 4
			break
		}
	case 8:
		{
			res := int64(0)
			if vm.param(1, p1Mode) == vm.param(2, p2Mode) {
				res = 1
			}
			vm.setmem(vm.paramOut(3, p3Mode), res)
			vm.ip += 4
			break
		}
	case 9:
		{
			vm.relBaseOffset += vm.param(1, p1Mode)
			vm.ip += 2
			break
		}
	default:
		{
			fmt.Println("Unknown instruction ", instruction)
			vm.terminated = true
			close(vm.done)
			break
		}
	case 99:
		{
			vm.terminated = true
			close(vm.done)
			break
		}
	}
}

func (vm *vm13) run() *vm13 {
	for {
		vm.step()
		if vm.terminated {
			return vm
		}
	}
}

type tile13 struct {
	x, y   int64
	tileID int64
}

func compute13a(input string) int64 {
	vm := createVM13(parseInt64Array(input))
	go vm.run()
	tiles := make([]*tile13, 0)
	blockCount := int64(0)
	for {
		select {
		case x := <-vm.output:
			y := <-vm.output
			tileID := <-vm.output
			tile := &tile13{x, y, tileID}
			tiles = append(tiles, tile)
			if tileID == 2 {
				blockCount++
			}
			break
		case <-vm.done:
			return blockCount
		}
	}
}

func compute13b(input string) int64 {
	mem := parseInt64Array(input)
	mem[0] = 2
	vm := createVM13(mem)
	go vm.run()
	score := int64(0)
	paddleX := int64(0)
	for {
		select {
		case x := <-vm.output:
			y := <-vm.output
			tileID := <-vm.output
			if x == -1 && y == 0 {
				score = tileID
			}
			if tileID == 3 {
				paddleX = x
			}
			if tileID == 4 {
				// Whenever the tileid is the ball, update the input to match the ball
				if x < paddleX {
					vm.input <- -1
				} else if x > paddleX {
					vm.input <- 1
				} else {
					vm.input <- 0
				}
			}
			break
		case <-vm.done:
			return score
		}
	}
	return 0
}

func main() {
	fmt.Println("Result A", compute13a(load("../inputs/13.txt")))
	fmt.Println("Result B", compute13b(load("../inputs/13.txt")))
}
