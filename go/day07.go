package main

import "fmt"

type vm struct {
	memory     []int
	ip         int
	terminated bool
	input      chan int
	output     chan int
}

func createVM(memory []int) *vm {
	memCopy := append(memory[:0:0], memory...)
	// Channels are buffered witht two elements as otherwise there could be a
	// deadlock situation waiting for write into a channel
	return &vm{memCopy, 0, false, make(chan int, 2), make(chan int, 2)}
}

func (vm *vm) param(idx int, mode int) int {
	if mode == 0 {
		return vm.memory[vm.memory[vm.ip+idx]]
	} else {
		return vm.memory[vm.ip+idx]
	}
}

func (vm *vm) step() {
	instruction := vm.memory[vm.ip]
	opCode := instruction % 100
	p1Mode := (instruction / 100) % 10
	p2Mode := (instruction / 1000) % 10
	switch opCode {
	case 1:
		{
			vm.memory[vm.param(3, 1)] = vm.param(1, p1Mode) + vm.param(2, p2Mode)
			vm.ip += 4
			break
		}
	case 2:
		{
			vm.memory[vm.param(3, 1)] = vm.param(1, p1Mode) * vm.param(2, p2Mode)
			vm.ip += 4
			break
		}
	case 3:
		{
			vm.memory[vm.param(1, 1)] = <-vm.input
			vm.ip += 2
			break
		}
	case 4:
		{
			vm.output <- vm.memory[vm.param(1, 1)]
			vm.ip += 2
			break
		}
	case 5:
		{
			if vm.param(1, p1Mode) != 0 {
				vm.ip = vm.param(2, p2Mode)
			} else {
				vm.ip += 3
			}
			break
		}
	case 6:
		{
			if vm.param(1, p1Mode) == 0 {
				vm.ip = vm.param(2, p2Mode)
			} else {
				vm.ip += 3
			}
			break
		}
	case 7:
		{
			if vm.param(1, p1Mode) < vm.param(2, p2Mode) {
				vm.memory[vm.param(3, 1)] = 1
			} else {
				vm.memory[vm.param(3, 1)] = 0
			}
			vm.ip += 4
			break
		}
	case 8:
		{
			if vm.param(1, p1Mode) == vm.param(2, p2Mode) {
				vm.memory[vm.param(3, 1)] = 1
			} else {
				vm.memory[vm.param(3, 1)] = 0
			}
			vm.ip += 4
			break
		}
	case 99:
		{
			vm.terminated = true
			break
		}
	}
}

func (vm *vm) run() *vm {
	for {
		vm.step()
		if vm.terminated {
			return vm
		}
	}
}

func run2(values []int, memory []int) int {
	vms := make([]*vm, 5)
	for i := 0; i < 5; i++ {
		vms[i] = createVM(memory)
		go vms[i].run()
		vms[i].input <- values[i]
	}
	vms[0].input <- 0
	output := 0
	for {
		select {
		case a := <-vms[0].output:
			vms[1].input <- a
		case b := <-vms[1].output:
			vms[2].input <- b
		case c := <-vms[2].output:
			vms[3].input <- c
		case d := <-vms[3].output:
			vms[4].input <- d
		case e := <-vms[4].output:
			output = e
			vms[0].input <- e
		default:
		}
		if vms[0].terminated && vms[1].terminated && vms[2].terminated && vms[3].terminated && vms[4].terminated {
			return output
		}
	}
}

func run1(values []int, memory []int) int {
	vms := make([]*vm, 5)
	for i := 0; i < 5; i++ {
		vms[i] = createVM(memory)
		go vms[i].run()
		vms[i].input <- values[i]
	}
	vms[0].input <- 0
	for {
		select {
		case a := <-vms[0].output:
			vms[1].input <- a
		case b := <-vms[1].output:
			vms[2].input <- b
		case c := <-vms[2].output:
			vms[3].input <- c
		case d := <-vms[3].output:
			vms[4].input <- d
		case e := <-vms[4].output:
			return e
		}
	}
}

type runFunc func([]int) int

func permuteMax(min int, max int, f runFunc) int {
	maxValue := 0
	for a := min; a < max; a++ {
		for b := min; b < max; b++ {
			if b == a {
				continue
			}
			for c := min; c < max; c++ {
				if c == b || c == a {
					continue
				}
				for d := min; d < max; d++ {
					if d == c || d == b || d == a {
						continue
					}
					for e := min; e < max; e++ {
						if e == d || e == c || e == b || e == a {
							continue
						}
						output := f([]int{a, b, c, d, e})
						if output > maxValue {
							maxValue = output
						}
					}
				}
			}
		}
	}
	return maxValue
}

func compute1(input string) int {
	memory := parseIntArray(input)
	return permuteMax(0, 5, func(values []int) int { return run1(values, memory) })
}

func compute2(input string) int {
	memory := parseIntArray(input)
	return permuteMax(5, 10, func(values []int) int { return run2(values, memory) })
}

func main() {
	test(compute1, "3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0", 43210)
	test(compute1, "3,23,3,24,1002,24,10,24,1002,23,-1,23,101,5,23,23,1,24,23,23,4,23,99,0,0", 54321)
	test(compute1, "3,31,3,32,1002,32,10,32,1001,31,-2,31,1007,31,0,33,1002,33,7,33,1,33,31,31,1,32,31,31,4,31,99,0,0,0", 65210)
	fmt.Println(compute1(load("../inputs/07.txt")))
	test(compute2, "3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5", 139629729)
	test(compute2, "3,52,1001,52,-5,52,3,53,1,52,56,54,1007,54,5,55,1005,55,26,1001,54,-5,54,1105,1,12,1,53,54,53,1008,54,0,55,1001,55,1,55,2,53,55,53,4,53,1001,56,-1,56,1005,56,6,99,0,0,0,0,10", 18216)
	fmt.Println(compute2(load("../inputs/07.txt")))
}
