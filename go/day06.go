package main

import (
	"fmt"
	"strings"
)

type node06 struct {
	name     string
	children []string
	parent   string
}

type tree06 struct {
	nodes map[string]*node06
	root  string
}

func (node *node06) String() string {
	return fmt.Sprintf("[%s %s %s]", node.name, node.parent, node.children)
}

func (tree *tree06) getOrAdd(nodeName string) *node06 {
	node, ok := tree.nodes[nodeName]
	if ok {
		return node
	}
	node = new(node06)
	node.name = nodeName
	node.children = make([]string, 0)
	tree.nodes[nodeName] = node
	return node
}

func parse(input string) *tree06 {
	lines := strings.Split(input, "\n")
	tree := new(tree06)
	tree.nodes = make(map[string]*node06, 0)
	tree.root = "COM"
	for _, line := range lines {
		link := strings.Split(line, ")")
		left := tree.getOrAdd(link[0])
		right := tree.getOrAdd(link[1])
		right.parent = left.name
		left.children = append(left.children, right.name)
	}
	return tree
}

func (tree *tree06) countRec(name string) int {
	node := tree.nodes[name]
	sum := 0
	for _, child := range node.children {
		sum += tree.countRec(child)
	}
	// Count myself upwards
	parent := node.parent
	for {
		if len(parent) == 0 {
			break
		}
		sum++
		parent = tree.nodes[parent].parent
	}
	return sum
}

func (tree *tree06) count01() int {
	return tree.countRec(tree.root)
}

func (tree *tree06) rootList(source string) []string {
	parent := tree.nodes[source].parent
	list := make([]string, 0)
	for {
		if len(parent) == 0 {
			return list
		}
		list = append(list, parent)
		parent = tree.nodes[parent].parent
	}
}

func (tree *tree06) transferCount(from string, to string) int {
	froot := tree.rootList(from)
	troot := tree.rootList(to)
	// remove common prefix
	for {
		if froot[len(froot)-1] != troot[len(troot)-1] {
			break
		}
		froot = froot[:len(froot)-1]
		troot = troot[:len(troot)-1]
	}
	return len(froot) + len(troot)
}

func compute1(input string) int {
	tree := parse(input)
	return tree.count01()
}

func compute2(input string) int {
	tree := parse(input)
	return tree.transferCount("YOU", "SAN")
}

func main() {
	test(compute1, "COM)B\nB)C\nC)D\nD)E\nE)F\nB)G\nG)H\nD)I\nE)J\nJ)K\nK)L", 42)
	fmt.Println(compute1(load("../inputs/06.txt")))
	test(compute2, "COM)B\nB)C\nC)D\nD)E\nE)F\nB)G\nG)H\nD)I\nE)J\nJ)K\nK)L\nK)YOU\nI)SAN", 4)
	fmt.Println(compute2(load("../inputs/06.txt")))
}
