package main

import (
	"fmt"
)

type img11 struct {
	image      map[string]int64
	x          int
	y          int
	dir        int // 0 up 1 right 2 down 3 left
	minX, maxX int
	minY, maxY int
}

type vm11 struct {
	memMap        map[int64]int64
	ip            int64
	terminated    bool
	input         chan int64
	output        chan int64
	done          chan bool
	relBaseOffset int64
}

func createVM11(memory []int64) *vm11 {
	memMap := make(map[int64]int64)
	for idx, value := range memory {
		memMap[int64(idx)] = value
	}
	return &vm11{memMap,
		0,
		false,
		make(chan int64, 2),
		make(chan int64, 2),
		make(chan bool),
		0}
}

func (vm *vm11) mem(idx int64) int64 {
	return vm.memMap[idx]
}

func (vm *vm11) setmem(idx int64, value int64) {
	vm.memMap[idx] = value
}

func (vm *vm11) param(idx int64, mode int64) int64 {
	switch mode {
	case 0:
		{
			// reference
			return vm.mem(vm.mem(vm.ip + idx))
		}
	case 1:
		{
			// immediate
			return vm.mem(vm.ip + idx)
		}
	case 2:
		{
			// relative mode
			return vm.mem(vm.relBaseOffset + vm.mem(vm.ip+idx))
		}
	default:
		{
			fmt.Println("mode ", mode, " NYI")
			return 0
		}
	}
}

func (vm *vm11) paramOut(idx int64, mode int64) int64 {
	switch mode {
	case 0:
		{
			// reference
			return vm.mem(vm.ip + idx)
		}
	case 1:
		{
			// immediate
			return vm.mem(vm.ip + idx)
		}
	case 2:
		{
			// relative mode
			return vm.relBaseOffset + vm.mem(vm.ip+idx)
		}
	default:
		{
			fmt.Println("mode ", mode, " NYI")
			return 0
		}
	}
}

func (vm *vm11) step() {
	instruction := vm.mem(vm.ip)
	opCode := instruction % 100
	p1Mode := (instruction / 100) % 10
	p2Mode := (instruction / 1000) % 10
	p3Mode := (instruction / 10000) % 10
	// fmt.Println(instruction, opCode, p1Mode, p2Mode, p3Mode, vm.mem(vm.ip), vm.mem(vm.ip+1), vm.mem(vm.ip+2), vm.mem(vm.ip+3))
	switch opCode {
	case 1:
		{
			vm.setmem(vm.paramOut(3, p3Mode), vm.param(1, p1Mode)+vm.param(2, p2Mode))
			vm.ip += 4
			break
		}
	case 2:
		{
			vm.setmem(vm.paramOut(3, p3Mode), vm.param(1, p1Mode)*vm.param(2, p2Mode))
			vm.ip += 4
			break
		}
	case 3:
		{
			vm.setmem(vm.paramOut(1, p1Mode), <-vm.input)
			vm.ip += 2
			break
		}
	case 4:
		{
			vm.output <- vm.param(1, p1Mode)
			vm.ip += 2
			break
		}
	case 5:
		{
			if vm.param(1, p1Mode) != 0 {
				vm.ip = vm.param(2, p2Mode)
			} else {
				vm.ip += 3
			}
			break
		}
	case 6:
		{
			if vm.param(1, p1Mode) == 0 {
				vm.ip = vm.param(2, p2Mode)
			} else {
				vm.ip += 3
			}
			break
		}
	case 7:
		{
			res := int64(0)
			if vm.param(1, p1Mode) < vm.param(2, p2Mode) {
				res = 1
			}
			vm.setmem(vm.paramOut(3, p3Mode), res)
			vm.ip += 4
			break
		}
	case 8:
		{
			res := int64(0)
			if vm.param(1, p1Mode) == vm.param(2, p2Mode) {
				res = 1
			}
			vm.setmem(vm.paramOut(3, p3Mode), res)
			vm.ip += 4
			break
		}
	case 9:
		{
			vm.relBaseOffset += vm.param(1, p1Mode)
			vm.ip += 2
			break
		}
	default:
		{
			fmt.Println("Unknown instruction ", instruction)
			vm.terminated = true
			close(vm.done)
			break
		}
	case 99:
		{
			vm.terminated = true
			close(vm.done)
			break
		}
	}
}

func (vm *vm11) run() *vm11 {
	for {
		vm.step()
		if vm.terminated {
			return vm
		}
	}
}

func coord(x int, y int) string {
	return fmt.Sprintf("%d,%d", x, y)
}

func (img *img11) pos() string {
	return coord(img.x, img.y)
}

func (img *img11) current() int64 {
	return img.image[coord(img.x, img.y)]
}

func (img *img11) paint(color int64) {
	img.image[coord(img.x, img.y)] = color
}

func (img *img11) rotate(dir int64) {
	if dir == 0 {
		img.dir = (img.dir + 1) % 4
	} else {
		img.dir = (img.dir + 4 - 1) % 4
	}
}

func (img *img11) move() {
	switch img.dir {
	case 0:
		img.y--
		break
	case 1:
		img.x++
		break
	case 2:
		img.y++
		break
	case 3:
		img.x--
		break
	default:
		panic("Unknown direction")
	}
	img.minX = min(img.minX, img.x)
	img.maxX = max(img.maxX, img.y)
	img.minY = min(img.minY, img.x)
	img.maxY = max(img.maxY, img.y)
}

func compute11(input string, start int64) *img11 {
	vm := createVM11(parseInt64Array(input))
	go vm.run()
	img := &img11{
		make(map[string]int64),
		0, 0,
		0,
		0, 0, 0, 0,
	}
	// Input the initial panel
	vm.input <- start
	for {
		select {
		case color := <-vm.output:
			dir := <-vm.output
			img.paint(color)
			img.rotate(dir)
			img.move()
			vm.input <- img.current()
			break
		case <-vm.done:
			return img
		}
	}
	return nil
}

func compute11a(input string) int {
	img := compute11(input, 0)
	return len(img.image)
}

func compute11b(input string) {
	img := compute11(input, 1)
	// Compute the image
	for y := img.minY; y <= img.maxY; y++ {
		line := fmt.Sprintf("%04d", y)
		for x := img.maxX; x >= img.minX; x-- {
			c := " "
			if img.image[coord(x, y)] != 0 {
				c = "#"
			}
			line += c
		}
		fmt.Println(line)
	}
}

func main() {
	fmt.Println("A result", compute11a(load("../inputs/11.txt")))
	compute11b(load("../inputs/11.txt"))
}
